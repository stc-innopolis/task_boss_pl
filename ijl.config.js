const pkg = require('./package');

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${process.env.NODE_ENV === 'development' ? pkg.name : 'task.boss'}/${
                process.env.VERSION || pkg.version
            }/`,
        },
    },
    features: {
        'task-boss': {
            'depts.search': true,
            'depts.new-flow': true,
            'tasks.search': true,
        },
    },
    config: {
        'task-boss.api': '/api',
        'task-boss.default.page': 'depts',
    },
};

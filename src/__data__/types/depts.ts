export type AsyncData<T> = {
    data: T;
    errors: any[];
    warnings: any[];
};

export type Department = {
    id: string;
    name: string;
    responsible: User;
    notes: string;
};

export type User = {
    id: string;
    family: string;
    name: string;
    secondName: string;
    phone: string;
    email: string;
};

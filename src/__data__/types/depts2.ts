export type AsyuncData<T> = {
    body: T;
    errors: any[];
    warnings: any[];
};

export type Department = {
    id: string;
    parentId: string;
    name: string;
    qty: number;
    responsible: User;
    tasks: Tasks;
    notes: string;
};

export type Tasks = {
    inWork: number;
    newTasks: number;
};

export type User = {
    id: number;
    family: string;
    name: string;
    secondName: string;
    phone: string;
    email: string;
};

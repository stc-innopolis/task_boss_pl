import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './root';

const loginRootSelector = createSelector(rootSelector, (state) => state.login);

export const loading = createSelector(loginRootSelector, (state) => state.loading);
export const error = createSelector(loginRootSelector, (state) => state.error);
export const data = createSelector(loginRootSelector, (state) => state.data);

import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './root';

const editTaskProgressRootSelector = createSelector(rootSelector, (state) => state.editTaskProgress);

export const data = createSelector(editTaskProgressRootSelector, (state) => state.data);

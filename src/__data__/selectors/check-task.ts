import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './root';

const checkTaskRootSelector = createSelector(rootSelector, (state) => state.checkTask);

export const ids = createSelector(checkTaskRootSelector, (state) => state.ids);
export const dept = createSelector(checkTaskRootSelector, (state) => {
    const data = Object.entries(state.ids)[0];
    if (!data) {
        return '';
    }
    const dept = data[1];
    return dept;
});

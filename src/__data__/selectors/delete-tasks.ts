import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './root';

const deleteTasksRootSelector = createSelector(rootSelector, (state) => state.deleteTasks);

export const data = createSelector(deleteTasksRootSelector, (state) => state.data);
export const loading = createSelector(deleteTasksRootSelector, (state) => state.loading);
export const error = createSelector(deleteTasksRootSelector, (state) => state.error);

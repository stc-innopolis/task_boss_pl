import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './root';

const deptsCreatorRootSelector = createSelector(rootSelector, (state) => state.deptCreator);

export const form = createSelector(deptsCreatorRootSelector, (state) => state.form);
export const peoples = createSelector(deptsCreatorRootSelector, (state) => state.peoples);
export const step = createSelector(deptsCreatorRootSelector, (state) => state.step);
export const data = createSelector(deptsCreatorRootSelector, (state) => state.data);
export const loading = createSelector(deptsCreatorRootSelector, (state) => state.loading);
export const error = createSelector(deptsCreatorRootSelector, (state) => state.error);
export const success = createSelector(deptsCreatorRootSelector, (state) => state.success);

import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './root';

const signUpRootSelector = createSelector(rootSelector, (state) => state.signUp);

export const loading = createSelector(signUpRootSelector, (state) => state.loading);
export const error = createSelector(signUpRootSelector, (state) => state.error);
export const data = createSelector(signUpRootSelector, (state) => state.data);

import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './root';
import { ids } from './check-task';
import { data as tasksData } from './tasks';

const editTaskRootSelector = createSelector(rootSelector, (state) => state.editTask);

export const loading = createSelector(editTaskRootSelector, (state) => state.loading);
export const error = createSelector(editTaskRootSelector, (state) => state.error);
export const data = createSelector(editTaskRootSelector, (state) => state.data);
export const got = createSelector(editTaskRootSelector, (state) => state.got);
export const editedTask = createSelector(ids, tasksData, (ids, tasksData) => {
    const id = Object.keys(ids)[0];
    if (!id) {
        return null;
    }
    let task = null;
    tasksData.forEach((data) => {
        const curTask = data.data.find((task) => task.id === id);
        if (!task && curTask) {
            task = curTask;
        }
    });
    return task;
});

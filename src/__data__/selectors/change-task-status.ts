import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './root';

const rootChangeTaskStatusSelector = createSelector(rootSelector, (state) => state.changeTaskStatus);

export const data = createSelector(rootChangeTaskStatusSelector, (state) => state.data);
export const loading = createSelector(rootChangeTaskStatusSelector, (state) => state.loading);
export const error = createSelector(rootChangeTaskStatusSelector, (state) => state.error);

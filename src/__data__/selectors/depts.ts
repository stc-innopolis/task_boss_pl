import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './root';

const deptsRootSelector = createSelector(rootSelector, (state) => state.depts);

export const loading = createSelector(deptsRootSelector, (state) => state.loading);
export const error = createSelector(deptsRootSelector, (state) => state.error);
export const data = createSelector(deptsRootSelector, (state) => state.data);

import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './root';

const createTaskRootSelector = createSelector(rootSelector, (state) => state.createTask);

export const loading = createSelector(createTaskRootSelector, (state) => state.loading);
export const error = createSelector(createTaskRootSelector, (state) => state.error);
export const data = createSelector(createTaskRootSelector, (state) => state.data);
export const got = createSelector(createTaskRootSelector, (state) => state.got);

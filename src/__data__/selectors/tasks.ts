import { createSelector } from '@reduxjs/toolkit';

import { rootSelector } from './root';

const tasksRootSelector = createSelector(rootSelector, (state) => state.tasks);

export const loading = createSelector(tasksRootSelector, (state) => state.loading);
export const error = createSelector(tasksRootSelector, (state) => state.error);
const search = createSelector(tasksRootSelector, (state) => state.search);
export const data = createSelector(tasksRootSelector, search, (state, search) => state.data.map((dept) => ({
    ...dept,
    data: dept.data.filter((t) => t.task.toLowerCase().includes(search.toLowerCase())),
})));

import { getConfig } from '@ijl/cli';

const config = getConfig();

export const baseApiURL = config['task-boss.api'];

export const URLs = {
    root: {
        url: '/task-boss/',
    },
    login: {
        url: '/task-boss/login',
    },
    signUp: {
        url: '/task-boss/sign-up',
    },
    depts: {
        url: '/task-boss/depts',
    },
    tasks: {
        url: '/task-boss/tasks',
    },
    api: {
        getDepts: '/depts',
        postDeptsCreate: '/depts/create',
        getPeoples: '/peoples',
        postLogin: '/login',
        postSignUp: '/sign-up',
        getTasks: '/tasks',
        createTask: '/tasks/create',
        editTask: '/tasks/edit',
        deleteTasks: '/tasks/delete',
        changeTaskStatus: '/tasks/change-status',
    },
};

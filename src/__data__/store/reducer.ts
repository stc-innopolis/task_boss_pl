import { deptsApi } from '../api/depts';

import { reducer as deptsReducer } from './features/depts';
import { reducer as loginReducer } from './features/login';
import { reducer as signUpReducer } from './features/signUp';
import { reducer as deptCreatorReducer } from './features/dept-creator';
import { peoplesApi } from '../api/peoples';
import { reducer as tasksReducer } from './features/tasks';
import { reducer as createTaskReducer } from './features/create-task';
import { reducer as editTaskReducer } from './features/edit-task';
import { reducer as checkTaskReducer } from './features/check-task';
import { reducer as deleteTasksReducer } from './features/delete-tasks';
import { reducer as changeTaskStatusReducer } from './features/change-task-status';
import { reducer as editTaskProgressReducer } from './features/edit-task-progress';

export const reducer = {
    [peoplesApi.reducerPath]: peoplesApi.reducer,
    [deptsApi.reducerPath]: deptsApi.reducer,
    depts: deptsReducer,
    deptCreator: deptCreatorReducer,
    login: loginReducer,
    signUp: signUpReducer,
    tasks: tasksReducer,
    createTask: createTaskReducer,
    editTask: editTaskReducer,
    checkTask: checkTaskReducer,
    deleteTasks: deleteTasksReducer,
    changeTaskStatus: changeTaskStatusReducer,
    editTaskProgress: editTaskProgressReducer,
};

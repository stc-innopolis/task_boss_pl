export const asyncFetchHandlers = {
    fetch(state) {
        state.loading = true;
    },
    success(state, action) {
        state.loading = false;
        state.data = action.payload;
    },
    error(state, action) {
        state.loading = false;
        state.error = action.payload;
    },
};

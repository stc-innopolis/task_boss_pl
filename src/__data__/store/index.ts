import { configureStore } from '@reduxjs/toolkit';

import { peoplesApi } from '../api/peoples';
import { deptsApi } from '../api/depts';

import { reducer } from './reducer';

const store = configureStore({
    reducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(peoplesApi.middleware).concat(deptsApi.middleware),
});
export type RootState = ReturnType<typeof store.getState>;

export default store;

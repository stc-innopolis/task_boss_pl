import { createSlice } from '@reduxjs/toolkit';

type signUpStore = {
    loading: boolean;
    error: string;
    data: string;
};

const initialState: signUpStore = {
    loading: false,
    error: null,
    data: null,
};

const { actions, reducer } = createSlice({
    name: 'signUp',
    initialState,
    reducers: {
        fetch(state) {
            state.loading = true;
        },
        success(state, action) {
            state.loading = false;
            state.data = action.payload;
        },
        error(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
    },
});

export { actions, reducer };

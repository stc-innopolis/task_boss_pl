import { createSlice } from '@reduxjs/toolkit';
import { User } from '../../types/depts2';

type deptCreatorType = {
    form: Record<string, string | number>,
    peoples: User[],
    error: string,
    data: string,
    loading: boolean,
    step: number,
    success: boolean,
}

const initialState: deptCreatorType = {
    form: {},
    peoples: [],
    error: null,
    data: null,
    loading: false,
    step: 1,
    success: false,
};

const { actions, reducer } = createSlice({
    name: 'deptCreator',
    initialState,
    reducers: {
        setFormField(state, { payload: { name, value } }) {
            state.form[name] = value;
        },
        setPeoples(state, { payload: { data } }) {
            state.peoples = [...state.peoples, data];
        },
        removePeoples(state, { payload: { id } }) {
            state.peoples = state.peoples.filter((i) => i.id !== id);
        },
        setStep(state, action) {
            state.step = action.payload;
        },
        fetch(state) {
            state.loading = true;
        },
        success(state, action) {
            state.loading = false;
            state.data = action.payload.body;
            state.success = action.payload.success;
            state.form = {};
            state.peoples = [];
            state.step = 1;
        },
        error(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
        resetSuccess(state) {
            state.success = false;
        },
    },
});

export { actions, reducer };

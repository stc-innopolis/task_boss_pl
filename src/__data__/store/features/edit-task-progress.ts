import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    data: {},
};

const { actions, reducer } = createSlice({
    name: 'editTaskProgress',
    initialState,
    reducers: {
        fetch(state, { payload: { id } }) {
            state.data[id] = {
                loading: true,
            };
        },
        success(state, { payload: { id } }) {
            state.data[id] = {
                loading: false,
            };
        },
    },
});

export { actions, reducer };

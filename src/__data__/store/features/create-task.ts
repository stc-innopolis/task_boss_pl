import { createSlice } from '@reduxjs/toolkit';

import { Task } from '../../model/task';

type TasksStore = {
  loading: boolean,
  data: Task,
  error: string,
  got: boolean,
}

const initialState: TasksStore = {
    loading: false,
    data: null,
    error: null,
    got: false,
};

export const { actions, reducer } = createSlice({
    name: 'createTask',
    initialState,
    reducers: {
        fetch(state) {
            state.loading = true;
        },
        success(state, action) {
            state.loading = false;
            state.data = action.payload;
            state.got = true;
        },
        error(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
        reset(state) {
            state.got = false;
        },
    },
});

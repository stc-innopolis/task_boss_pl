import { createSlice } from '@reduxjs/toolkit';

import { DepartmentTasks } from '../../model/task';
import { asyncFetchHandlers } from '../common';

type TasksStore = {
    loading: boolean;
    data: DepartmentTasks[];
    error: string;
    search: string;
};

const initialState: TasksStore = {
    loading: false,
    data: [],
    error: null,
    search: '',
};

export const { actions, reducer } = createSlice({
    name: 'tasks',
    initialState,
    reducers: {
        ...asyncFetchHandlers,
        search(state, action) {
            state.search = action.payload;
        },
        updateTask(state, { payload: task }) {
            state.data = state.data.map((dept) => {
                if (dept.data.find((t) => t.id === task.id)) {
                    return {
                        ...dept,
                        data: dept.data.map((t) => {
                            if (t.id === task.id) {
                                return task;
                            }
                            return t;
                        }),
                    };
                }
                return dept;
            });
        },
    },
});

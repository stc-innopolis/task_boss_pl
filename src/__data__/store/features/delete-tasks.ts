import { createSlice } from '@reduxjs/toolkit';

import { asyncFetchHandlers } from '../common';

const initialState = {
    loading: false,
    data: null,
    error: null,
};

export const { actions, reducer } = createSlice({
    name: 'deleteTasks',
    initialState,
    reducers: {
        ...asyncFetchHandlers,
    },
});

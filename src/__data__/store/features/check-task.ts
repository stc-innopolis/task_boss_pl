import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    ids: {},
};

export const { actions, reducer } = createSlice({
    name: 'checkTask',
    initialState,
    reducers: {
        toogle(state, { payload: { id, dept } }) {
            if (state.ids[id]) {
                delete state.ids[id];
            } else {
                state.ids[id] = dept;
            }
        },
    },
});

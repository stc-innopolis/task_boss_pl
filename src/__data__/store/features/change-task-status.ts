import { createSlice } from '@reduxjs/toolkit';

import { asyncFetchHandlers } from '../common';

const initialState = {
    data: {
        id: '',
        status: '',
    },
    loading: false,
    error: null,
};

export const { actions, reducer } = createSlice({
    name: 'changeTaskStatus',
    initialState,
    reducers: {
        ...asyncFetchHandlers,
    },
});

import { createSlice } from '@reduxjs/toolkit';

import { Department } from '../../types/depts2';

type DeptsStore = {
    loading: boolean;
    data: Department[];
    error: string;
};

const initialState: DeptsStore = {
    loading: false,
    data: null,
    error: null,
};

const { actions, reducer } = createSlice({
    name: 'Depts',
    initialState,
    reducers: {
        fetch(state) {
            state.loading = true;
        },
        success(state, action) {
            state.loading = false;
            state.data = action.payload;
        },
        error(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
    },
});

export { actions, reducer };

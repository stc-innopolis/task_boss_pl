import { createApi } from '@reduxjs/toolkit/query/react';
import { authAxios } from '../../utils/axios';

import { Department, AsyuncData } from '../types/depts2';
import { URLs } from '../urls';

export const deptsApi = createApi({
    reducerPath: 'deptsApi',
    baseQuery: authAxios,
    keepUnusedDataFor: 1,
    endpoints: (builder) => ({
        getDepts: builder.query<AsyuncData<Department[]>, void>({
            query: () => URLs.api.getDepts,
        }),
    }),
});

export const { useGetDeptsQuery } = deptsApi;

import { createApi } from '@reduxjs/toolkit/query/react';
import { authAxios } from '../../utils/axios';

import { User, AsyuncData } from '../types/depts2';
import { URLs } from '../urls';

export const peoplesApi = createApi({
    reducerPath: 'peoplesApi',
    baseQuery: authAxios,
    keepUnusedDataFor: 1,
    endpoints: (builder) => ({
        getPeoples: builder.query<AsyuncData<User[]>, void>({
            query: () => URLs.api.getPeoples,
        }),
    }),
});

export const { useGetPeoplesQuery } = peoplesApi;

import { authAxios } from '../../utils/axios';
import { URLs } from '../urls';
import { actions } from '../store/features/tasks';


export const setSearch = (value) => (
    actions.search(value)
);

export const getTasks = () => async (dispatch) => {
    dispatch(actions.fetch());
    try {
        const response = await authAxios.get(URLs.api.getTasks);
        dispatch(actions.success(response.data.body));
    } catch (e) {
        dispatch(actions.error(e.message));
    }
};

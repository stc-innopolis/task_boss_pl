import { authAxios } from '../../utils/axios';
import { URLs } from '../urls';
import { actions } from '../store/features/depts';

export const getDepts = () => async (dispatch) => {
    dispatch(actions.fetch());

    try {
        const response = await authAxios({
            method: 'GET',
            url: URLs.api.getDepts,
        });

        dispatch(actions.success(response.data.body));
    } catch (e) {
        dispatch(actions.error(e.message));
    }
};

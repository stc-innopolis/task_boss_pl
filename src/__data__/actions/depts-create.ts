import axios from '../../utils/axios';
import { URLs } from '../urls';
import { actions } from '../store/features/dept-creator';

export const createDept = (data) => async (dispatch) => {
    dispatch(actions.fetch());

    try {
        const response = await axios({
            method: 'post',
            url: URLs.api.postDeptsCreate,
            data: { data },
        });

        dispatch(actions.success(response.data));
    } catch (e) {
        dispatch(actions.error(e.message));
    }
};

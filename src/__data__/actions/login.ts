import { URLs } from '../urls';
import { actions } from '../store/features/login';
import axios, { authAxios } from '../../utils/axios';

export const userLoginFetch = (login, password) => async (dispatch) => {
    dispatch(actions.fetch());
    try {
        const { data } = await axios({
            url: URLs.api.postLogin,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            },
            data: { login, password },
        });

        if (data.errors?.length) {
            const [errorMessage] = data.errors;
            dispatch(actions.error(errorMessage));
        } else {
            dispatch(actions.success(data.body.token));
            localStorage.setItem('task-boss-token', data.body.token);
            authAxios.defaults.headers.Authorization = `Bearer ${localStorage.getItem('task-boss-token')}`;
        }
    } catch (e) {
        dispatch(actions.error(e.message));
    }
};

import { URLs } from '../urls';
import { actions } from '../store/features/signUp';
import axios, { authAxios } from '../../utils/axios';

export const userSignUpFetch = (login, password, mail) => async (dispatch) => {
    dispatch(actions.fetch());
    try {
        const { data } = await axios({
            url: URLs.api.postSignUp,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            },
            data: { login, password, mail },
        });

        if (data.errors?.length) {
            const [errorMessage] = data.errors;
            dispatch(actions.error(errorMessage));
        } else {
            dispatch(actions.success(data.body.token));
            localStorage.setItem('task-boss-token', data.body.token);
            authAxios.defaults.headers.Authorization = `Bearer ${localStorage.getItem('task-boss-token')}`;
        }
    } catch (e) {
        dispatch(actions.error(e.message));
    }
};

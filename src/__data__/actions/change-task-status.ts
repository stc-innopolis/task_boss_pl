import axios from '../../utils/axios';
import { URLs } from '../urls';
import { actions } from '../store/features/change-task-status';

export const deleteTasks = (data) => async (dispatch) => {
    dispatch(actions.fetch());
    try {
        const response = await axios({
            method: 'POST',
            url: URLs.api.changeTaskStatus,
            data,
        });
        dispatch(actions.success(response.data.body));
    } catch (e) {
        dispatch(actions.error(e.message));
    }
};


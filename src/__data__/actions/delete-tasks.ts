import axios from '../../utils/axios';
import { URLs } from '../urls';
import { actions } from '../store/features/delete-tasks';

export const deleteTasks = (data) => async (dispatch) => {
    dispatch(actions.fetch());
    try {
        const response = await axios({
            method: 'POST',
            url: URLs.api.deleteTasks,
            data,
        });
        dispatch(actions.success(response.data.body));
    } catch (e) {
        dispatch(actions.error(e.message));
    }
};

import { Task } from '../model/task';
import { actions } from '../store/features/create-task';
import axios from '../../utils/axios';
import { URLs } from '../urls';

export const createTask = (data: Omit<Task, 'id' | 'number' | 'lastChanged' | 'status'>) => async (dispatch) => {
    dispatch(actions.fetch());
    try {
        const response = await axios({
            method: 'POST',
            url: URLs.api.createTask,
            data,
        });
        dispatch(actions.success(response.data.body));
    } catch (e) {
        dispatch(actions.error(e.message));
    }
};

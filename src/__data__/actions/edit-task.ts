import { Task } from '../model/task';
import { actions } from '../store/features/edit-task';
import { actions as progressActions } from '../store/features/edit-task-progress';
import axios from '../../utils/axios';
import { URLs } from '../urls';

export const editTask = (data: Task) => async (dispatch) => {
    dispatch(actions.fetch());
    dispatch(progressActions.fetch(data));
    try {
        const response = await axios({
            method: 'POST',
            url: URLs.api.editTask,
            data,
        });
        dispatch(actions.success(response.data.body));
    } catch (e) {
        dispatch(actions.error(e.message));
    }
    dispatch(progressActions.success(data));
};

export type DepartmentTasks = {
    label: string;
    data: Task[];
};

export enum TaskStatus {
    inProgress = 'inProgress',
    open = 'open',
    needInfo = 'needInfo',
}

export enum TaskPriority {
    high = 'high',
    middle = 'middle',
    low = 'low',
}

export type Task = {
    id: string;
    number: number | string;
    task: string;
    status: TaskStatus;
    priority: TaskPriority;
    performer: string;
    deadline: number;
    lastChanged: number;
    description: string;
};

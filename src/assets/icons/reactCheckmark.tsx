import React from 'react';

export const LockCheckmark = (props: React.SVGProps<SVGSVGElement>) => (
    <svg viewBox="0 0 10 15" fill="none" xmlns="http://www.w3.org/2000/svg" width="18" height="30" {...props}>
        <g clipPath="url(#clip0)">
            <rect width="10" height="15" fill="white" />
            <path
                d="M9.95908 3.63953L3.7588 12.9399C3.70418 13.0219 3.61575 13.0219 3.56127 12.9399L0.0407423 7.65897C-0.0137435 7.57743 -0.0137435 7.44479 0.0407423 7.36287L0.896183 6.07971C0.950796 5.99798 1.03922 5.99798 1.09371 6.07971L3.66016 9.92919L8.90624 2.06008C8.96098 1.97835 9.04915 1.97835 9.10377 2.06008L9.95908 3.34324C10.0137 3.42496 10.0137 3.55741 9.95908 3.63953Z"
                fill="#9AC73F"
            />
        </g>
        <defs>
            <clipPath id="clip1">
                <rect width="10" height="15" fill="white" />
            </clipPath>
        </defs>
    </svg>
);

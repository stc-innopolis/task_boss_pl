export const theme = {
    colors: {
        black: '#000000',
        white: '#FFFFFF',
        orange: '#E8973E',
        action: {
            main: '#9AC73F',
            mainHover: '#7A9B37',
            secondary: '#333333',
            secondaryHover: '#6A5137',
        },
        gray: {
            dark: '#E0E0E0',
            light: '#F5F5F5',
        },
        text: {
            common: '#656565',
            muted: '#A7A7A7',
        },
        danger: {
            main: '#F44336',
            secondary: '#FCEDEE',
        },
        modal: {
            background: '#000000b3',
        },
    },
};

export type WithTheme = {
    theme: typeof theme;
};

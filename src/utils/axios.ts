import axios from 'axios';

import { baseApiURL } from '../__data__/urls';

const instance = axios.create({
    baseURL: baseApiURL,
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
    },
});

export const authAxios = axios.create({
    baseURL: baseApiURL,
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${localStorage.getItem('task-boss-token')}`,
    },
    withCredentials: true,
});

export default instance;

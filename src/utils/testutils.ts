import { act } from 'react-dom/test-utils';
import _ from 'lodash';

let counter = 0;

const getResponce = (arr, config) => {
    const index = arr.findIndex(
        ([method, url, params]) => config.url.includes(url)
            && config.method.toUpperCase() === method
            && (!config.params
                || config.params === _.isEqual(config.params, params)),
    );

    if (index !== -1) {
        const restResps = [...arr];
        restResps.splice(index, 1);

        return [arr[index], restResps];
    }
    throw new Error(`Запрос не обработан${JSON.stringify(config, null, 4)}`);
};

export const multipleRequest = async (mock, responses, strict = true) => {
    let resps = [...responses];
    await act(async () => {
        await mock.onAny().reply((config) => {
            counter += 1;
            if (strict) {
                const [method, url, params, ...response] = responses.shift();

                if (
                    config.url.includes(url)
                    && config.method.toUpperCase() === method
                ) {
                    if (!config.params || _.isEqual(config.params, params)) {
                        return response;
                    }

                    console.error(`
                        [${counter}] Не обработанный запрос
                        Параметры не совпадают
                        ${JSON.stringify(params.value(), null, 2)}
                        ${JSON.stringify(config.params.value(), null, 2)}
                    `);

                    return [500, {}];
                }

                console.error(`
                    [${counter}] Не обработанный запрос
                    Адрес и(или) метод не совпадает
                    ${JSON.stringify({ url: config.url, method: config.method }, null, 2)}
                    возможно адрес   ->${config.url}<-
                    не соответствует ->${url}<-
                `);

                return [500, {}];
            }
            const [[, , , ...response], newResponses] = getResponce(
                resps,
                config,
            );
            resps = newResponses;
            return response;
        });
    });
};

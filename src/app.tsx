import React from 'react';
import { Provider } from 'react-redux';
import { getHistory } from '@ijl/cli';
import { Router } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';

import Dashboard from './dashboard';
import { Globalstyle } from './app-style';
import { theme } from './app-theme';
import store from './__data__/store';

const history = getHistory();

const App = () => (
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <Router history={history}>
                <Globalstyle />
                <Dashboard />
            </Router>
        </ThemeProvider>
    </Provider>
);

export default App;

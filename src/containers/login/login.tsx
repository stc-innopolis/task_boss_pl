import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation, Link } from 'react-router-dom';
import { getConfigValue } from '@ijl/cli';

import { URLs } from '../../__data__/urls';
import { Logo } from '../../assets';
import { LabeledInput } from '../../components/labeled-input';
import { userLoginFetch } from '../../__data__/actions/login';
import * as selectors from '../../__data__/selectors';

import { Wrapper, StyledForm, StyledLogo, StyledHeader, Button, StyledText } from './style';

const difaultPageKey = getConfigValue('task-boss.default.page');

const useInput = (initValue = '') => {
    const [value, setValue] = useState(initValue);
    const [isDirty, setDirty] = useState(false);
    const isEmpty = !value;
    const onChange = (e) => {
        setValue(e.target.value);
    };

    const onBlur = () => {
        setDirty(true);
    };

    return {
        value,
        onChange,
        onBlur,
        isDirty,
        isEmpty,
    };
};

function Login() {
    const dispatch = useDispatch();
    const [validation, setValidation] = useState<{ [key: string]: string }>({});
    const login = useInput();
    const pass = useInput();
    const loading = useSelector(selectors.login.loading);
    const data = useSelector(selectors.login.data);
    const error = useSelector(selectors.login.error);
    const history = useHistory();
    const location: any = useLocation();

    useEffect(() => {
        if (login.value) {
            setValidation((v) => ({ ...v, login: null }));
        }
        if (pass.value) {
            setValidation((v) => ({ ...v, pass: null }));
        }
    }, [login.value, pass.value, setValidation]);

    useEffect(() => {
        if (data && !loading) {
            const { from } = location.state || { from: { pathname: URLs[difaultPageKey].url } };
            history.replace(from);
        }
    }, [data, location, history]);

    function handleSubmit(event): void {
        event.preventDefault();

        if (!login.value) {
            setValidation({ login: 'Поле обязательно для заполнения' });
            return;
        }
        if (!pass.value) {
            setValidation({ pass: 'Поле обязательно для заполнения' });
            return;
        }
        dispatch(userLoginFetch(login.value, pass.value));
    }

    return (
        <Wrapper>
            <StyledLogo>
                <img src={Logo} alt="logo" />
            </StyledLogo>
            <StyledHeader>Sign In</StyledHeader>
            {error && <span style={{ color: 'red' }}>{error}</span>}
            <StyledForm onSubmit={handleSubmit}>
                <LabeledInput
                    id="login"
                    name="login"
                    placeholder="Login"
                    value={login.value}
                    onChange={login.onChange}
                    onBlur={login.onBlur}
                    error={validation.login}
                    disabled={loading}
                />
                <LabeledInput
                    id="password"
                    name="password"
                    type="password"
                    placeholder="Password"
                    value={pass.value}
                    onChange={pass.onChange}
                    onBlur={pass.onBlur}
                    error={validation.pass}
                    disabled={loading}
                />
                <Button type="submit" disabled={loading}>
                    Next
                </Button>
                <StyledText>
                    Don &apos; t have an account?
                    <Link to={URLs.signUp.url}>Sign Up</Link>
                </StyledText>
            </StyledForm>
        </Wrapper>
    );
}

export default Login;

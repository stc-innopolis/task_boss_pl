import styled from 'styled-components';

export const Wrapper = styled.div`
    margin: 1em;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    font-family: 'Roboto', sans-serif;
    padding: 10px;
`;

export const StyledForm = styled.form`
    display: flex;
    flex-direction: column;
`;

export const StyledHeader = styled.p`
    font-size: 25px;
    font-family: 'Times New Roman';
    text-align: center;
    font-size: 25px;
    font-weight: 700;
`;

export const StyledText = styled.p`
    text-align: center;
    font-size: 15px;
    font-family: 'Roboto', sans-serif;
`;

export const Button = styled.button`
    background-color: ${(props) => props.theme.colors.action.secondary};
    width: 400px;
    color: ${(props) => props.theme.colors.white};
    text-align: center;
    display: block;
    font-size: 18px;
    border-radius: 10px;
    padding-top: 12px;
    padding-bottom: 12px;
`;

export const StyledLogo = styled.div`
    width: 50%;
    height: 30%;
    margin-left: auto;
    margin-right: auto;
    display: flex;
    justify-content: center;
    align-items: center;
`;

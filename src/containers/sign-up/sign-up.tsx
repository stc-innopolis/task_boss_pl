import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { useHistory, useLocation, Link } from 'react-router-dom';
import { getConfigValue } from '@ijl/cli';

import { URLs } from '../../__data__/urls';
import { Logo } from '../../assets';
import { Wrapper, Img, ContainerSignIn, Header, Button, MessageError } from './style';
import { LabeledInput } from '../../components/labeled-input';
import { TooltipPassword } from '../../components/tooltip-password';
import { userSignUpFetch } from '../../__data__/actions/signUp';
import * as selectors from '../../__data__/selectors';

const difaultPageKey = getConfigValue('task-boss.default.page');

function SignUp() {
    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm({
        defaultValues: {
            login: '',
            password: '',
            repeatPassword: '',
            mail: '',
        }
    });
    const [isOnMouseEnter, setOnMouseEnter] = useState(false);
    const [passValue, setPassValue] = useState('');
    const dispatch = useDispatch();
    const loading = useSelector(selectors.signUp.loading);
    const data = useSelector(selectors.signUp.data);
    const error = useSelector(selectors.signUp.error);
    const history = useHistory();
    const location: any = useLocation();

    useEffect(() => {
        if (data && !loading) {
            history.replace({ pathname: URLs[difaultPageKey].url });
        }
    }, [data, location, history]);

    const onSubmit = (data) => {
        dispatch(userSignUpFetch(data.login, data.password, data.mail));
    };

    return (
        <Wrapper>
            <Img src={Logo} />
            <Header>Sign Up</Header>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Controller
                    name="login"
                    control={control}
                    rules={{
                        required: { value: true, message: 'поле обязательно для заполнения' },
                        maxLength: { value: 20, message: 'максимальная длина пароля 20 символов' },
                        minLength: { value: 4, message: 'поле должно содержать не менее 4 символов' },
                    }}
                    render={({ field: { ref, ...rest } }) => (
                        <LabeledInput
                            id="login"
                            placeholder="login"
                            error={errors.login && errors.login.message}
                            inputRef={ref}
                            {...rest}
                            disabled={loading}
                        />
                    )}
                />
                {isOnMouseEnter && <TooltipPassword passValue={passValue} visible />}
                <Controller
                    name="password"
                    control={control}
                    rules={{
                        required: { value: true, message: 'поле обязательно для заполнения' },
                        pattern: {
                            value: /(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[#?!@$%^&*-])(?=.{8,})/i,
                            message: 'пароль не соответствует требованиям',
                        },
                        onBlur: () => setOnMouseEnter(false),
                        onChange: (e) => setPassValue(e.target.value),
                    }}
                    render={({ field: { ref, onBlur, onChange, value, ...rest } }) => (
                        <LabeledInput
                            id="password"
                            placeholder="Password"
                            type="password"
                            error={errors.password && errors.password.message}
                            inputRef={ref}
                            onFocus={() => setOnMouseEnter(true)}
                            onBlur={onBlur}
                            onChange={onChange}
                            {...rest}
                            disabled={loading}
                        />
                    )}
                />
                <Controller
                    name="repeatPassword"
                    control={control}
                    rules={{
                        required: { value: true, message: 'поле обязательно для заполнения' },
                        pattern: { value: new RegExp(`${passValue}$`, 'i'), message: 'пароли не совпадают' },
                    }}
                    render={({ field: { ref, ...rest } }) => (
                        <LabeledInput
                            id="repeatPassword"
                            placeholder="Repeat password"
                            type="password"
                            error={errors.repeatPassword && errors.repeatPassword.message}
                            inputRef={ref}
                            {...rest}
                            disabled={loading}
                        />
                    )}
                />

                <Controller
                    name="mail"
                    control={control}
                    rules={{
                        required: { value: true, message: 'поле обязательно для заполнения' },
                        pattern: { value: /\S+@\S+\.\S+/i, message: 'некорректный ввод почтового ящика' },
                    }}
                    render={({ field: { ref, ...rest } }) => (
                        <LabeledInput
                            id="mail"
                            placeholder="Email address"
                            error={errors.mail && errors.mail.message}
                            inputRef={ref}
                            {...rest}
                            disabled={loading}
                        />
                    )}
                />
                <ContainerSignIn>
                    Already have an account?
                    <Link to={URLs.login.url}>Sign In</Link>
                </ContainerSignIn>
                <Button type="submit" disabled={loading}>Next</Button>
                {error
                    && <MessageError>{error}</MessageError>}
            </form>
        </Wrapper>
    );
}

export default SignUp;

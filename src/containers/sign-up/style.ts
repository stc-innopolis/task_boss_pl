import styled from 'styled-components';

export const Wrapper = styled.div`
    flex-direction: column;
    display: flex;
    justify-content: space-around;
    text-align: center;
    font-size: 25px;
    font-weight: 700;
    color: ${(props) => props.theme.colors.action.secondary};
    height: 800px;
    width: 400px;
    margin-left: auto;
    margin-right: auto;
`;

export const Img = styled.img`
    width: 50%;
    height: 30%;
    margin-left: auto;
    margin-right: auto;
`;

export const Input = styled.input`
    height: 35px;
    width: 400px;
    font-size: 20px;
    font-family: 'Roboto';
    border-color: ${(props) => props.theme.colors.action.secondary};
    border-width: 3px;
`;

export const Button = styled.button`
    background-color: ${(props) => props.theme.colors.action.secondary};
    width: 400px;
    color: ${(props) => props.theme.colors.white};
    text-align: center;
    display: block;
    font-size: 18px;
    border-radius: 10px;
    padding-top: 12px;
    padding-bottom: 12px;
`;

export const ContainerSignIn = styled.div`
    flex-direction: row;
    font-size: 15px;
    font-family: 'Roboto';
    font-style: normal;
    padding-top: 12px;
    padding-bottom: 12px;
`;

export const Header = styled.div`
    font-size: 25px;
    font-family: 'Roboto';
`;

export const Label = styled.label``;

export const MessageError = styled.div`
    font-family: 'Roboto';
    font-style: normal;
    color: ${(props) => props.theme.colors.danger.main};
    font-size: 18px;
`;

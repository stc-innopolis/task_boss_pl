import DeptName from './form-step1/dept-name';
import DeptInfo from './form-step2/dept-info';
import Peoples from './form-step3/peoples';

export { DeptInfo, Peoples, DeptName };

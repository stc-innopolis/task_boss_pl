import styled from 'styled-components';

export const ErrorMsg = styled.div`
font-family: 'Roboto';
font-style: normal;
color: ${(props) => props.theme.colors.danger.main};
font-size: 18px;
`;

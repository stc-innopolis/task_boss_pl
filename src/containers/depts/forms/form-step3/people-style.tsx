import styled from 'styled-components';

import Button from '../../../../components/button';
import DangerButton from '../../../../components/danger-button';

export const CreateButtonExtended = styled(Button)`
    background-image: none;
    margin-top: 10px;
    text-indent: 0;
    margin-left: auto;
`;

export const Container = styled.div`
    width: 100%;
    max-width: 100%;
    margin-top: 25px;
`;

export const Table = styled.table`
    width: 100%;
    border-collapse: collapse;
    margin-top: 20px;
`;

export const Thead = styled.thead`
    border-bottom: 1px solid ${(props) => props.theme.colors.black};
    text-align: center;
    &:after {
        line-height: 3px;
        content: '.';
        color: ${(props) => props.theme.colors.white};
        display: block;
    }
`;

export const Th = styled.th`
    font-family: Montserrat;
    font-weight: 600;
    font-size: 14px;
    border-bottom: 2px solid ${(props) => props.theme.colors.gray.dark};
    height: 45px;
`;

export const Tbody = styled.tbody`
    text-align: center;
`;

export const Td = styled.td`
    font-family: Montserrat;
    font-size: 10px;
    border: 1px solid ${(props) => props.theme.colors.gray.dark};
    color: ${(props) => props.theme.colors.text.common};
    height: 49px;
`;

export const Tr = styled.tr`
    background-color: ${(props) => props.theme.colors.white};
    &:nth-child(odd) {
        background-color: ${(props) => props.theme.colors.gray.light};
    }
`;

export const TrashButton = styled.button`
    background: transparent;
    border: none;
    width: 30px;
    height: 30px;
    &:hover {
        cursor: pointer;
    }
`;

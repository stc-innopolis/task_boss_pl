import React, { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import i18next from 'i18next';

import { trash } from '../../../../assets';

import * as selectors from '../../../../__data__/selectors';
import { actions } from '../../../../__data__/store/features/dept-creator';
import SelectComponent from '../../../../components/select';
import { useGetPeoplesQuery } from '../../../../__data__/api/peoples';

import {
    Container,
    Table,
    Thead,
    Th,
    Tbody,
    Tr,
    Td,
    CreateButtonExtended,
    TrashButton,
} from './people-style';
import { StyledLabel } from '../form-step2/dept-info-style';

const customStyles = {
    container: (base) => ({
        ...base,
        width: '400px',
    }),
};

const Peoples = () => {
    const form = useSelector(selectors.deptsCreator.form);
    const peoplesList = useSelector(selectors.deptsCreator.peoples);
    const dispatch = useDispatch();
    const { data: peoples, error, isLoading } = useGetPeoplesQuery();
    const peoplesOptions = useMemo(() => peoples?.body.map((d) => ({
        value: d.id,
        label: `${d.family} ${d.name.substring(0, 1)}. ${d.secondName.substring(0, 1)}.`,
    })), [peoples]);

    const handleAddRow = (e) => {
        e.preventDefault();
        const id = form.peoplesIdFio;
        if (form.peoplesIdFio) {
            if (peoplesList.findIndex((i) => i.id === id) === -1) {
                dispatch(actions.setPeoples({ data: peoples?.body.find((i) => i.id === id) }));
            }
        }
    };

    const handleChange = ({ value, label }) => {
        dispatch(actions.setFormField({ name: 'peoplesIdFio', value }));
        dispatch(actions.setFormField({ name: 'peoplesFio', value: label }));
    };

    const handleRemove = ({ currentTarget }) => {
        const { value } = currentTarget;
        const id: number = Number(value);
        dispatch(actions.removePeoples({ id }));
    };

    if (isLoading || !peoples) {
        return (
            <Container>
                <span>loading...</span>
            </Container>
        );
    }

    if (error) {
        return (
            <span style={{ color: 'red' }}>{typeof error === 'string' ? error : 'Что-то пошло не так!'}</span>
        );
    }

    return (
        <Container>
            <StyledLabel>{i18next.t('task.boss.depts.create.form.peoples.field.fio')}</StyledLabel>
            <SelectComponent
                onChange={handleChange}
                options={peoplesOptions}
                value={{ value: form.peoplesIdFio || '', label: form.peoplesFio || '' }}
                styles={customStyles}
            />
            <CreateButtonExtended
                id="add-button"
                onClick={handleAddRow}
                variant="create"
                className="createButton"
            >
                {i18next.t('task.boss.depts.create.form.peoples.add')}
            </CreateButtonExtended>
            <Table>
                <Thead>
                    <tr>
                        <Th>{i18next.t('task.boss.depts.peoples.table.head.family')}</Th>
                        <Th>{i18next.t('task.boss.depts.peoples.table.head.name')}</Th>
                        <Th>{i18next.t('task.boss.depts.peoples.table.head.secondName')}</Th>
                        <Th>{i18next.t('task.boss.depts.peoples.table.head.phone')}</Th>
                        <Th>{i18next.t('task.boss.depts.peoples.table.head.email')}</Th>
                        <Th />
                    </tr>
                </Thead>
                <Tbody>
                    {peoplesList.length > 0 ? (
                        peoplesList.map((item) => (
                            <Tr key={item.id}>
                                <Td>{item.family}</Td>
                                <Td>{item.name}</Td>
                                <Td>{item.secondName}</Td>
                                <Td>{item.phone}</Td>
                                <Td>{item.email}</Td>
                                <Td>
                                    <TrashButton type="button" value={item.id} onClick={handleRemove} title="Удалить">
                                        <img src={trash} width="100%" alt="Иконка корзины" />
                                    </TrashButton>
                                </Td>
                            </Tr>
                        ))
                    ) : (
                        <Tr>
                            <Td colSpan={6}>Нет данных...</Td>
                        </Tr>
                    )}
                </Tbody>
            </Table>
        </Container>
    );
};

export default Peoples;

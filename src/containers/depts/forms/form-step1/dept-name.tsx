import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import i18next from 'i18next';

import { StyledFormRow, StyledLabel, StyledInput } from '../form-step2/dept-info-style';
import { ErrorMsg } from '../forms-style';
import * as selectors from '../../../../__data__/selectors';
import { actions } from '../../../../__data__/store/features/dept-creator';

type errorType = {
    field: string,
    msg: string,
};

const DeptName: React.FC<{ error: errorType }> = ({ error }) => {
    const form = useSelector(selectors.deptsCreator.form);
    const dispatch = useDispatch();

    const handleChange = ({ target }) => {
        const { value } = target;
        dispatch(actions.setFormField({ name: 'name', value }));
    };

    return (
        <>
            <StyledFormRow>
                <StyledLabel>{i18next.t('task.boss.depts.create.form.field.name')}</StyledLabel>
                <StyledInput onChange={handleChange} value={form.name || ''} style={{ borderColor: error?.field === 'name' && 'red' }} />
            </StyledFormRow>
            {error && error.field === 'name' && <ErrorMsg>{error.msg}</ErrorMsg>}
        </>
    );
};

export default DeptName;

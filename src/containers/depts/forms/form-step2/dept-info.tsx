import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import i18next from 'i18next';

import { StyledFormRow, StyledLabel, StyledInput } from './dept-info-style';
import { ErrorMsg } from '../forms-style';
import * as selectors from '../../../../__data__/selectors';
import { actions } from '../../../../__data__/store/features/dept-creator';
import SelectComponent from '../../../../components/select';
import { useGetPeoplesQuery } from '../../../../__data__/api/peoples';
import { useGetDeptsQuery } from '../../../../__data__/api/depts';

const customStyles = {
    container: (base) => ({
        ...base,
        width: '99%',
    }),
};

type errorType = {
    field: string,
    msg: string,
};

const DeptInfo: React.FC<{ error: errorType }> = ({ error }) => {
    const form = useSelector(selectors.deptsCreator.form);
    const dispatch = useDispatch();
    const { data: peoples } = useGetPeoplesQuery();
    const { data: depts } = useGetDeptsQuery();

    const peoplesOptions = useMemo(() => peoples?.body?.map((d) => ({
        value: d.id,
        label: `${d.family} ${d.name.substring(0, 1)}. ${d.secondName.substring(0, 1)}.`,
    })), [peoples]);

    const deptsOptions = useMemo(() => depts?.body?.map((d) => ({
        value: d.id,
        label: d.name,
    })), [depts]);

    const handleParentDeptChange = ({ value, label }) => {
        dispatch(actions.setFormField({ name: 'parentDept', value }));
        dispatch(actions.setFormField({ name: 'parentDeptName', value: label }));
    };

    const handleFioChange = ({ value, label }) => {
        dispatch(actions.setFormField({ name: 'idFio', value }));
        dispatch(actions.setFormField({ name: 'fio', value: label }));
    };

    const handleEmailChange = ({ target }) => {
        const { value } = target;
        dispatch(actions.setFormField({ name: 'email', value }));
    };

    const handleNoteChange = ({ target }) => {
        const { value } = target;
        dispatch(actions.setFormField({ name: 'note', value }));
    };

    return (
        <>
            <StyledFormRow>
                <StyledLabel>{i18next.t('task.boss.depts.create.form.field.parentDept')}</StyledLabel>
                <SelectComponent
                    id="dept-select"
                    options={deptsOptions}
                    onChange={handleParentDeptChange}
                    value={{ value: form.parentDept || '', label: form.parentDeptName || '' }}
                    styles={customStyles}
                />
            </StyledFormRow>
            <StyledFormRow>
                <StyledLabel>{i18next.t('task.boss.depts.create.form.field.fio')}</StyledLabel>
                <SelectComponent
                    id="peoples-select"
                    options={peoplesOptions}
                    onChange={handleFioChange}
                    value={{ value: form.idFio || '', label: form.fio || '' }}
                    styles={customStyles}
                />
                {error && error.field === 'fio' && <ErrorMsg>{error.msg}</ErrorMsg>}
            </StyledFormRow>
            <StyledFormRow>
                <StyledLabel>{i18next.t('task.boss.depts.create.form.field.email')}</StyledLabel>
                <StyledInput data-test="mail-input" onChange={handleEmailChange} value={form.email || ''} />
                {error && error.field === 'email' && <ErrorMsg>{error.msg}</ErrorMsg>}
            </StyledFormRow>
            <StyledFormRow>
                <StyledLabel>{i18next.t('task.boss.depts.create.form.field.note')}</StyledLabel>
                <StyledInput onChange={handleNoteChange} value={form.note || ''} />
            </StyledFormRow>
        </>
    );
};

export default DeptInfo;

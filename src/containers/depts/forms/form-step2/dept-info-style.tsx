import styled from 'styled-components';

export const StyledLabel = styled.label`
    display: block;
    font-size: 12px;
    color: ${(props) => props.theme.colors.text.common};
`;

export const StyledInput = styled.input.attrs({
    type: 'text',
})`
    width: 98%;
    height: 30px;
    border: 1px solid ${(props) => props.theme.colors.gray.dark};
    margin-top: 5px;
`;

export const StyledFormRow = styled.div`
    margin-top: 25px;
`;

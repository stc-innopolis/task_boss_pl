import styled from 'styled-components';

export const StyledUl = styled.ul`
    list-style-type: none;
    margin: 0;
    padding: 4px;
`;

export const StyledLi = styled.li`
    display: inline;
`;

export const StyledRadio = styled.input.attrs({
    type: 'radio',
})`
    display: inline;
    opacity: 0;
    background-color: ${(props) => props.theme.colors.gray.dark};
    &:checked + label {
        background: ${(props) => props.theme.colors.white};
        color: ${(props) => props.theme.colors.black};
    }
`;

export const StyledRadioLabel = styled.label`
    color: ${(props) => props.theme.colors.text.common};
    display: inline;
    padding: 5px 22px;
    margin-left: -22px;
    cursor: pointer;
`;

import React, { useCallback, useState } from 'react';

import { DeptInfo, Peoples } from '../forms';
import { StyledRadio, StyledRadioLabel, StyledLi, StyledUl } from './style';

const TabButton = ({ selected, title, onTabClick }) => (
    <>
        <StyledRadio id={title} name="tabs" onClick={onTabClick} defaultChecked={selected} />
        <StyledRadioLabel htmlFor={title}>{title}</StyledRadioLabel>
    </>
);

const TabTitle = ({ selected, title, onTabClick, tab }) => {
    const onClick = () => onTabClick(tab);

    return (
        <StyledLi>
            <TabButton title={title} onTabClick={onClick} selected={selected} />
        </StyledLi>
    );
};

const Tabs = ({ children }) => (
    <div>
        <StyledUl>{children}</StyledUl>
    </div>
);

enum DepInfoTab {
    info,
    people,
}

const tabs = {
    [DepInfoTab.info]: DeptInfo,
    [DepInfoTab.people]: Peoples,
};

const TabControl = () => {
    const [selectedTab, setSelected] = useState(DepInfoTab.info);
    const Component = tabs[selectedTab];

    const handleTabClick = useCallback(
        (tab) => {
            setSelected(tab);
        },
        [setSelected],
    );

    return (
        <>
            <Tabs>
                <TabTitle
                    title="Информация"
                    selected={selectedTab === DepInfoTab.info}
                    tab={DepInfoTab.info}
                    onTabClick={handleTabClick}
                />
                <TabTitle
                    title="Люди"
                    selected={selectedTab === DepInfoTab.people}
                    tab={DepInfoTab.people}
                    onTabClick={handleTabClick}
                />
            </Tabs>
            <div>
                {/* <Component /> */}
            </div>
        </>
    );
};

export default TabControl;

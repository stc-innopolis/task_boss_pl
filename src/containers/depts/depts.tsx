import React, { useState } from 'react';
import { getFeatures } from '@ijl/cli';

import Content from './content';
import CommonPage from '../../components/common-page';

const deptsSearchFeature = getFeatures('task-boss')['depts.search'];

const Depts = () => {
    const [filterValue, setFilterValue] = useState('');
    return (
        <CommonPage userName="Администратор" showSearch={deptsSearchFeature} onFilter={setFilterValue}>
            <Content filterValue={filterValue} />
        </CommonPage>
    );
};

export default Depts;

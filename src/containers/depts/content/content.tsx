import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import i18next from 'i18next';
import styled from 'styled-components';
import { getFeatures } from '@ijl/cli';

import Button from '../../../components/button';
import DeptsTableWrapper from './table';
import Modal from '../../../components/modal';
import Workflow from '../dept-create-workflow';
import { Wrapper } from './style';
import { Feature } from '../../../components/feature';
import * as selectors from '../../../__data__/selectors';

import { actions } from '../../../__data__/store/features/dept-creator';

const deptsNewFlow = getFeatures('task-boss')['depts.new-flow'];

const CreateButtonExtended = styled(Button)`
    margin-left: 55px;
    margin-top: 10px;
`;

const ActionButtonExtended = styled(Button)`
    margin-left: 19px;
    margin-top: 10px;
`;

const Content = ({ filterValue }) => {
    const [showModal, setShowModal] = useState(false);
    const success = useSelector(selectors.deptsCreator.success);
    const dispatch = useDispatch();
    useEffect(() => {
        if (success) {
            setShowModal(false);
            dispatch(actions.resetSuccess());
        }
    }, [success, dispatch]);

    function handleToggleModal() {
        setShowModal(!showModal);
    }

    return (
        <Wrapper>
            {showModal && (
                <Modal onClose={handleToggleModal} title={i18next.t('task.boss.depts.modal.title')}>
                    <Feature featureName="depts.new-flow">
                        <Workflow />
                    </Feature>
                </Modal>
            )}
            {deptsNewFlow && (
                <CreateButtonExtended variant="create" onClick={handleToggleModal} id="create-dept" className="createButton">
                    {i18next.t('task.boss.buttons.create')}
                </CreateButtonExtended>
            )}
            <ActionButtonExtended variant="actions" className="actionButton">
                {i18next.t('task.boss.buttons.actions')}
            </ActionButtonExtended>
            <DeptsTableWrapper searchText={filterValue.toLowerCase()} />
        </Wrapper>
    );
};

export default Content;

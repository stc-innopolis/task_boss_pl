import styled from 'styled-components';

export const Button = styled.button`
    width: 122px;
    height: 34px;
    background-color: ${(props) => props.theme.colors.action.main};
    border-radius: 30px;
    border: 1px solid ${(props) => props.theme.colors.action.main};
    color: ${(props) => props.theme.colors.white};
    margin-top: auto;
    margin-top: 15px;
    margin-right: 10px;
    margin-left: auto;
    float: right;
    &:hover {
        background-color: ${(props) => props.theme.colors.action.mainHover};
    }
`;

export default Button;

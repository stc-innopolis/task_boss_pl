/* eslint-disable no-param-reassign */

import React from 'react';

import { useDeptsData } from './depts-data';
import { Department } from '../../../../__data__/types/depts2';
import { Container } from './style';
import TreeTable from './tree-table';

type Ext = {
    level: number;
    display: boolean;
    expandIco: boolean;
    leaf: boolean;
    subdepts: string[];
};

type ExtendedDepartment = Department & Ext;

type propsTypes = {
    searchText: string;
};

const setLevels = (dataset: ExtendedDepartment[], root = '0', level = 1) => {
    const tmp = [];
    dataset
        .filter((d) => d.parentId === root)
        .forEach((item) => {
            item.level = level;
            if (item.subdepts.length > 0) {
                tmp.push(item.id);
            }
        });

    level += 1;

    tmp.forEach((t) => {
        setLevels(dataset, t, level);
    });
};

const createTree = (dataset: ExtendedDepartment[], root: string) => {
    dataset.forEach((item) => {
        item.display = item.parentId === root;

        const tmp = [];

        dataset.filter((d) => item.id === d.parentId).forEach((m) => tmp.push(m.id));
        item.subdepts = tmp;
        if (tmp.length > 0) {
            item.expandIco = true;
            item.leaf = false;
        } else {
            item.expandIco = false;
            item.leaf = true;
        }
    });
};

const find = (arr, id) => {
    let ret = [];
    arr.forEach((item) => {
        if (item.parentId === id) {
            ret.push(item);
            const tmp = find(arr, item.id);
            if (tmp.length > 0) ret = ret.concat(tmp);
        }
    });

    return ret;
};

const sortArray = (dataset: ExtendedDepartment[]) => {
    let temp = [];

    dataset.forEach((item) => {
        temp.push(item);

        const ret = find(dataset, item.id);
        if (ret.length > 0) temp = temp.concat(ret);
    });

    return [...new Set(temp)];
};

export const init = (dataset) => {
    const root: string = dataset[0] === undefined ? '0' : dataset[0].parentId;
    const d = sortArray(dataset);
    createTree(d, root);
    setLevels(d, root);
    return d;
};

const DeptsTableWrapper: React.FC<propsTypes> = ({ searchText }) => {
    const [data, loading, error] = useDeptsData();

    if (loading || !data) {
        return (
            <Container>
                <span>loading...</span>
            </Container>
        );
    }

    if (error) {
        return (
            <Container>
                <span style={{ color: 'red' }}>{typeof error === 'string' ? error : 'Что-то пошло не так!'}</span>
            </Container>
        );
    }

    return (
        <TreeTable dataSet={init(data.filter((item) => !searchText || item.name.toLowerCase().includes(searchText)))} />
    );
};

export default DeptsTableWrapper;

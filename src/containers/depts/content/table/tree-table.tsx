import React, { useEffect, useState } from 'react';
import i18next from 'i18next';

import { Container, Table, Thead, Th, Tbody, Tr, Td, Circle, CheckBox, Wrapper, LeftBorder } from './style';
import { Span } from './span';

const TreeTable = ({ dataSet }) => {
    const [data, setData] = useState(dataSet.filter((item) => item.display));
    useEffect(() => {
        setData(dataSet.filter((item) => item.display));
    }, [dataSet, setData]);

    const hideSubdepts = (subDept) => {
        let currentRow;
        dataSet.forEach((item) => {
            if (item.id === subDept.id) {
                currentRow = item;
            }
        });

        currentRow.expandIco = true;
        const subDepts = subDept.subdepts;
        subDepts.forEach((s) => {
            const sub: any = dataSet.filter((d) => d.id === s);
            hideSubdepts(sub[0]);
            sub[0].display = false;
        });
    };

    const handleClick = (currentNode) => {
        let currentRow;
        dataSet.forEach((item) => {
            if (item.id === currentNode) {
                currentRow = item;
            }
        });

        if (currentRow.expandIco) {
            currentRow.expandIco = false;
            const subDepts = currentRow.subdepts;
            subDepts.forEach((s) => {
                const subDept: any = dataSet.filter((d) => d.id === s);
                subDept[0].display = true;
            });
        } else {
            currentRow.expandIco = true;
            const subDepts = currentRow.subdepts;
            subDepts.forEach((s) => {
                const subDept: any = dataSet.filter((d) => d.id === s);
                hideSubdepts(subDept[0]);
                subDept[0].display = false;
            });
        }

        setData(dataSet.filter((item) => item.display));
    };

    const itemListGenerator = (count: number) => {
        const itemList = [];

        for (let i = 1; i <= count - 2; i += 1) {
            itemList.push(<LeftBorder key={`${count}${i}`} level={count} order={i} />);
        }

        return itemList;
    };

    return (
        <Container>
            <Table>
                <Thead>
                    <tr>
                        <Th />
                        <Th>{i18next.t('task.boss.depts.table.head.name')}</Th>
                        <Th>{i18next.t('task.boss.depts.table.head.qty')}</Th>
                        <Th>{i18next.t('task.boss.depts.table.head.responsible')}</Th>
                        <Th>{i18next.t('task.boss.depts.table.head.tasks.inWork')}</Th>
                        <Th>{i18next.t('task.boss.depts.table.head.tasks.newTasks')}</Th>
                        <Th>{i18next.t('task.boss.depts.table.head.notes')}</Th>
                    </tr>
                </Thead>
                <Tbody>
                    {data.length > 0 ? (
                        data.map((item) => (
                            <Tr key={item.id} bias={item.level}>
                                {!item.leaf ? (
                                    <Td>
                                        <Wrapper level={item.level}>
                                            {itemListGenerator(item.level)}
                                            <Span
                                                currentNode={item.id}
                                                handleExpandClick={handleClick}
                                                icon={item.expandIco}
                                            />
                                            {' '}
                                            <CheckBox />
                                        </Wrapper>
                                    </Td>
                                ) : (
                                    <Td>
                                        <Wrapper level={item.level}>
                                            {itemListGenerator(item.level)}
                                            <Circle />
                                            {' '}
                                            <CheckBox />
                                        </Wrapper>
                                    </Td>
                                )}
                                <Td>{item.name}</Td>
                                <Td>{item.qty}</Td>
                                <Td>
                                    {item.responsible.family}
                                    {' '}
                                    {item.responsible.name.substring(0, 1)}
                                    {'. '}
                                    {item.responsible.secondName.substring(0, 1)}
                                    .
                                </Td>
                                <Td>{item.tasks.inWork}</Td>
                                <Td>{item.tasks.newTasks}</Td>
                                <Td>{item.notes}</Td>
                            </Tr>
                        ))
                    ) : (
                        <Tr>
                            <Td colSpan={7}>Нет данных...</Td>
                        </Tr>
                    )}
                </Tbody>
            </Table>
        </Container>
    );
};

export default TreeTable;

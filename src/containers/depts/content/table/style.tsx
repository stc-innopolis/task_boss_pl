import styled, { css } from 'styled-components';

import { circle, expand, minus } from '../../../../assets';

export const Container = styled.div`
    width: 85%;
    max-width: 85%;
    margin-top: 30px;
    margin-left: 55px;
`;

export const Table = styled.table`
    width: 100%;
    border-collapse: collapse;
    border-spacing: 11px 11px;
    border: 1px solid black;
`;

export const Thead = styled.thead`
    border: 1px solid white;
    &:after {
        line-height: 3px;
        content: '.';
        color: ${(props) => props.theme.colors.white};
        display: block;
    }
`;

export const Th = styled.th`
    font-family: Montserrat;
    font-weight: 550;
    font-size: 14px;
    height: 45px;
`;

export const Tbody = styled.tbody`
    text-align: center;
`;

export const Td = styled.td`
    font-family: Montserrat;
    font-size: 10px;
    border-bottom: 1px solid ${(props) => props.theme.black};
    border-top: 1px solid ${(props) => props.theme.black};
    color: ${(props) => props.theme.colors.text.common};
    height: 49px;
    vertical-align: middle;
    padding: 0;
`;

export const Tr = styled.tr<{ bias?: number }>`
    & > td:first-child {
        width: 80px;
        padding-left: ${(props) => (props.bias - 1) * 37}px;
        ${(props) => (props.bias > 1
        ? css`
                      border-top: none;
                      border-bottom: none;
                  `
        : '')}
    }
`;

export const Circle = styled.div`
    width: 17px;
    height: 17px;
    background: url(${circle}) no-repeat scroll 14px 14px;
    background-position: center;
    margin-left: 30px;
`;

export const Expand = styled(Circle)`
    background: url(${expand}) no-repeat scroll 14px 14px;
    background-position: center;
`;

export const Minus = styled(Circle)`
    background: url(${minus}) no-repeat scroll 14px 14px;
    background-position: center;
`;

export const CheckBox = styled.input.attrs({ type: 'checkbox' })`
    margin-left: 20px;
`;

export const Wrapper = styled.div<{ level?: number }>`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
    border-left: ${(props) => (props.level > 1 ? 1 : 0)}px solid  ${(props) => props.theme.black};
    border-bottom: ${(props) => (props.level > 1 ? 1 : 0)}px solid  ${(props) => props.theme.black};
    height: 100%;
`;

export const LeftBorder = styled.div<{ level?: number; order: number }>`
    position: relative;
    height: 100%;
    border-left: ${(props) => (props.level > 2 ? 1 : 0)}px solid  ${(props) => props.theme.black};
    left: ${(props) => -(props.order * 39 - props.order)}px;
`;

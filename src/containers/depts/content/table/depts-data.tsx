import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';

import { Department } from '../../../../__data__/types/depts2';
import { getDepts } from '../../../../__data__/actions/depts';
import * as selectors from '../../../../__data__/selectors';

export const useDeptsData = (): [Department[], boolean, string | boolean] => {
    const data = useSelector(selectors.depts.data);
    const loading = useSelector(selectors.depts.loading);
    const error = useSelector(selectors.depts.error);
    const success = useSelector(selectors.deptsCreator.success);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getDepts());
    }, [dispatch]);

    if (success) {
        dispatch(getDepts());
    }

    return [_.cloneDeep(data), loading, error];
};

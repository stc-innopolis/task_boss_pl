import React from 'react';

import { Expand, Minus } from './style';

export const Span = ({ currentNode, handleExpandClick, icon }) => {
    const handleClick = () => {
        handleExpandClick(currentNode);
    };

    return (
        <span onClick={handleClick} onKeyDown={handleClick} role="button" tabIndex={0}>
            {icon ? <Expand /> : <Minus />}
        </span>
    );
};

import styled from 'styled-components';

import NavigateButton from '../content/buttons/navigate-button';

export const StyledForm = styled.form`
    display: flex;
    flex-direction: column;
`;

export const Wrapper = styled.div`
    /* display: block; */
`;

export const NavigateButtonExtended = styled(NavigateButton)`
    background-color: ${(props) => props.theme.colors.orange};
    border: 1px solid ${(props) => props.theme.colors.orange};
    &:hover {
        background-color: ${(props) => props.theme.colors.orange};
    }
`;

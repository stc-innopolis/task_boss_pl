import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import i18next from 'i18next';

import { DeptInfo, Peoples, DeptName } from '../forms';
import * as selectors from '../../../__data__/selectors';
import NavigateButton from '../content/buttons/navigate-button';
import { actions } from '../../../__data__/store/features/dept-creator';
import { createDept } from '../../../__data__/actions/depts-create';

import { StyledForm, Wrapper, NavigateButtonExtended } from './style';

const steps = {
    1: DeptName,
    2: DeptInfo,
    3: Peoples,
};

const DeptCreate = () => {
    const [formError, setFormError] = useState(null);
    const step = useSelector(selectors.deptsCreator.step);
    const form = useSelector(selectors.deptsCreator.form);
    const peoples = useSelector(selectors.deptsCreator.peoples);
    const data = useSelector(selectors.deptsCreator.data);
    const loading = useSelector(selectors.deptsCreator.loading);
    const error = useSelector(selectors.deptsCreator.error);
    const dispatch = useDispatch();

    const checkName = () => {
        const valid = (('name' in form) && form?.name !== '');
        if (!valid) {
            setFormError({ field: 'name', msg: 'Поле название не должно быть пустым!' });
        }
        return valid;
    };

    const checkResponsible = () => {
        const valid = step === 2 ? 'idFio' in form : true;
        if (!valid) {
            setFormError({ field: 'fio', msg: 'Необходимо указать ФИО!' });
        }
        return valid;
    };

    const checkMail = () => {
        // eslint-disable-next-line max-len
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let valid = true;
        if ('email' in form) {
            valid = re.test(String(form.email).toLowerCase());
            if (!valid) {
                setFormError({ field: 'email', msg: 'Неверный формат email!' });
            }
        }
        return valid;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (step < 3) {
            if (step === 1) {
                if (checkName()) {
                    dispatch(actions.setStep(step + 1));
                    setFormError(null);
                }
            }
            if (step === 2) {
                if (checkResponsible() && checkMail()) {
                    dispatch(actions.setStep(step + 1));
                    setFormError(null);
                }
            }
        } else {
            const parentDept = form.parentDept ?? 0;
            dispatch(createDept({
                form: {
                    name: form.name,
                    parentDept,
                    idFio: form.idFio,
                    fio: form.fio,
                    email: form.email,
                    note: form.note,
                },
                peoples,
            }));
        }
    };

    const handleBack = () => {
        if (step > 1) {
            dispatch(actions.setStep(step - 1));
        }
    };

    const Step = steps[step];
    const isEndStep = step === 3;

    return (
        <StyledForm onSubmit={handleSubmit}>
            <Step error={formError} />
            <Wrapper>
                <NavigateButton
                    data-test="next-btn"
                    type="submit"
                >
                    {i18next.t(`task.boss.depts.create.form.${isEndStep ? 'submit' : 'next'}`)}
                </NavigateButton>
                {step > 1 && (
                    <NavigateButtonExtended
                        data-test="back-btn"
                        type="button"
                        onClick={handleBack}
                    >
                        {i18next.t('task.boss.depts.create.form.back')}
                    </NavigateButtonExtended>
                )}
            </Wrapper>
        </StyledForm>
    );
};

export default DeptCreate;

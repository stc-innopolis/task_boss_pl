import styled from 'styled-components';

export const Wrapper = styled.div`
    padding-top: 70px;
    padding-left: 120px;
`;

export const List = styled.ul`
    position: absolute;
    border-radius: 10%;
    list-style-type: none;
    z-index: 1000;
    margin: 0;
    padding: 0;
    left: 316px;
    top: 115px;
    width: 160px;
    height: 90px;
    background-color: ${({ theme }) => theme.colors.gray.dark};
    font-family: Montserrat;
    font-size: 10pt;
    font-weight: bold;
`;

export const Item = styled.li`
    position: relative;
    margin-left: 12px;
    margin-top: 10px;
`;

export const Action = styled.button`
    background: none;
    border: none;
    font: inherit;
    &:hover {
        color: ${({ theme }) => theme.colors.white};
    }
`;

import i18next from 'i18next';
import React, { useMemo, useEffect } from 'react';
import Select from 'react-select';
import { useForm, Controller } from 'react-hook-form';

import { theme } from '../../../../app-theme';
import { useGetDeptsQuery } from '../../../../__data__/api/depts';
import CircularLoader from '../../../../components/circular-loader';

import { Form, FormRow, Label, Input, InputDate, TextArea, StyledButton, ErrorLabel } from './style';

const priorityValuesArr = ['high', 'middle', 'low'];

const customStyles = (theme) => ({
    dropdownIndicator: (base) => ({
        ...base,
        color: theme.colors.action.main,
    }),
});

type TaskFormProps = {
    onClose: () => void,
    onSubmit: (data: any) => void,
    taskSelectors: {
        taskLoading: any,
        taskError: any,
        taskGot: any,
    },
    submitButtonLabel: string,
    task?: any,
    dept?: any,
};

const TaskForm: React.FC<TaskFormProps> = ({
    onClose,
    onSubmit,
    submitButtonLabel,
    taskSelectors: { taskLoading, taskError, taskGot },
    task,
    dept,
}) => {
    const { data: depts, isLoading } = useGetDeptsQuery();
    const deptsOptions = useMemo(() => depts?.body.map((d) => ({ value: d.name, label: d.name })), [depts]);
    const priorityValues = priorityValuesArr.map((p) => ({
        value: p,
        label: i18next.t(`task.boss.tasks.priority.${p}`),
    }));

    const { register, control, handleSubmit, formState: { errors }, setValue } = useForm({
        defaultValues: {
            task: task?.task || '',
            performer: task?.performer || '',
            dept: deptsOptions?.filter((option) => option.value === dept),
            priority: priorityValues.filter((option) => option.value === task?.priority),
            deadline: `${new Date(task?.deadline).getFullYear()}-${(new Date(task?.deadline).getMonth() + 1).toString().padStart(2, '0')}-${(new Date(task?.deadline).getDate()).toString().padStart(2, '0')}`,
            description: task?.description || '',
        },
    });

    useEffect(() => {
        if (!isLoading && depts) {
            setValue('dept', deptsOptions?.filter((option) => option.value === dept));
        }
    }, [isLoading, depts, deptsOptions, setValue]);

    if (taskGot && !taskError) {
        onClose();
    }

    return (
        <>
            {taskError && <div>{i18next.t('task.boss.error')}</div>}
            {taskLoading && <CircularLoader />}
            {!taskLoading && (
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <FormRow>
                        <Label>{i18next.t('task.boss.tasks.table.head.title')}</Label>
                        <Input
                            data-testid="task-form-task"
                            {...register('task', { required: i18next.t('task.boss.required.field').toString() })}
                        />
                        {errors.task && <ErrorLabel>{errors.task.message}</ErrorLabel>}
                    </FormRow>
                    <FormRow>
                        <Label>{i18next.t('task.boss.tasks.table.head.performer')}</Label>
                        <Input
                            data-testid="task-form-performer"
                            {...register('performer', { required: i18next.t('task.boss.required.field').toString() })}
                        />
                        {errors.performer && <ErrorLabel>{errors.performer.message}</ErrorLabel>}
                    </FormRow>
                    <FormRow>
                        <Label>{i18next.t('task.boss.tasks.modal.new.task.dept')}</Label>
                        <Controller
                            name="dept"
                            control={control}
                            rules={{ required: i18next.t('task.boss.required.field').toString() }}
                            render={({ field }) => (
                                <Select
                                    {...field}
                                    id="task-form-dept"
                                    styles={customStyles(theme)}
                                    options={deptsOptions}
                                />
                            )}
                        />
                        {errors.dept && <ErrorLabel>{(errors.dept as any).message}</ErrorLabel>}
                    </FormRow>
                    <FormRow>
                        <Label>{i18next.t('task.boss.tasks.table.head.priority')}</Label>
                        <Controller
                            name="priority"
                            control={control}
                            rules={{ required: i18next.t('task.boss.required.field').toString() }}
                            render={({ field }) => (
                                <Select
                                    {...field}
                                    id="task-form-priority"
                                    styles={customStyles(theme)}
                                    options={priorityValues}
                                />
                            )}
                        />
                        {errors.priority && <ErrorLabel>{(errors.priority as any).message}</ErrorLabel>}
                    </FormRow>
                    <FormRow>
                        <Label>{i18next.t('task.boss.tasks.table.head.deadline')}</Label>
                        <InputDate
                            data-testid="task-form-deadline"
                            {...register('deadline', { required: i18next.t('task.boss.required.field').toString() })}
                        />
                        {errors.deadline && <ErrorLabel>{errors.deadline.message}</ErrorLabel>}
                    </FormRow>
                    <FormRow>
                        <Label>{i18next.t('task.boss.tasks.modal.new.task.description')}</Label>
                        <TextArea
                            data-testid="task-form-description"
                            {...register('description', { required: i18next.t('task.boss.required.field').toString() })}
                        />
                        {errors.description && <ErrorLabel>{errors.description.message}</ErrorLabel>}
                    </FormRow>
                    <StyledButton data-testid="task-form-submit" variant="create" type="submit">
                        {submitButtonLabel}
                    </StyledButton>
                </Form>
            )}
        </>
    );
};

TaskForm.defaultProps = {
    task: {},
    dept: '',
};

export default TaskForm;

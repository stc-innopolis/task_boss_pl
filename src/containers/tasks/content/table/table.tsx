import React, { useMemo } from 'react';
import i18next from 'i18next';

import { Task } from '../../../../__data__/model/task';

import { Container, Table, Thead, TrHead, Th, Input, Tbody } from './style';
import TableRow from './table-row';

export const TableComponent: React.FC<{ data: Task[], dept: string }> = ({ data, dept }) => {
    const currentTime = useMemo(() => new Date().getTime(), []);
    return (
        <Container>
            <Table>
                <Thead>
                    <TrHead>
                        <Th>
                            <Input type="checkbox" />
                        </Th>
                        <Th>{i18next.t('task.boss.tasks.table.head.number')}</Th>
                        <Th>{i18next.t('task.boss.tasks.table.head.title')}</Th>
                        <Th>{i18next.t('task.boss.tasks.table.head.status')}</Th>
                        <Th>{i18next.t('task.boss.tasks.table.head.priority')}</Th>
                        <Th>{i18next.t('task.boss.tasks.table.head.performer')}</Th>
                        <Th>{i18next.t('task.boss.tasks.table.head.deadline')}</Th>
                        <Th>{i18next.t('task.boss.tasks.table.head.edited')}</Th>
                    </TrHead>
                </Thead>
                <Tbody>
                    {
                        data.map((item) => (
                            <TableRow key={item.id} item={item} currentTime={currentTime} dept={dept} />
                        ))
                    }
                </Tbody>
            </Table>
        </Container>
    );
};

import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import i18next from 'i18next';
import Select from 'react-select/creatable';

import { Task, TaskStatus } from '../../../../../__data__/model/task';
import { actions as checkActions } from '../../../../../__data__/store/features/check-task';
import { editTask } from '../../../../../__data__/actions/edit-task';
import { actions as tasksActions } from '../../../../../__data__/store/features/tasks';
import { actions as editTaskActions } from '../../../../../__data__/store/features/edit-task';
import * as selectors from '../../../../../__data__/selectors';
import LinearPreloader from '../../../../../components/linear-preloader';

import { Tr, Td, Strip, CheckboxWrapper, Input, TaskText, Status, Priority, Deadline, Description } from './style';

const convertUnixTimestampToDateTime = (timestamp) => {
    const date = new Date(timestamp);
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const year = date.getFullYear();
    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    return `${day}:${month}:${year} ${hours}:${minutes}`;
};

const CheckBox = ({ id, dept, onChange, value }) => {
    const handleChange = useCallback(() => {
        onChange(id, dept);
    }, [id, dept, onChange]);

    return (
        <Input
            id={`task-checkbox-${id}`}
            onChange={handleChange}
            style={{ marginTop: '13px' }}
            type="checkbox"
            checked={value || false}
        />
    );
};

const customStyles = () => ({
    dropdownIndicator: () => ({
        display: 'none',
    }),
});


type TableRowProps = {
    item: Task,
    currentTime: number,
    dept: string,
};

export const TableRow: React.FC<TableRowProps> = ({ item, currentTime, dept }) => {
    const options = useMemo(() => ['inProgress', 'open', 'needInfo'].map((p) => ({
        value: p,
        label: i18next.t(`task.boss.tasks.status.${p}`),
    })), []);

    const [isClicked, setIsClicked] = useState(false);

    const dispatch = useDispatch();
    const handleCheckTask = useCallback((id, dept) => {
        dispatch(checkActions.toogle({ id, dept }));
    }, [dispatch]);
    const checkedValues = useSelector(selectors.checkTask.ids);
    const progressData = useSelector(selectors.editTaskProgress.data);
    const editing = progressData[item.id]?.loading;
    const editedTask = useSelector(selectors.editTask.data);
    useEffect(() => {
        if (editedTask) {
            dispatch(tasksActions.updateTask(editedTask));
            dispatch(editTaskActions.reset());
        }
    }, [dispatch, editedTask]);

    return (
        <>
            <Tr key={item.id} isExpired={item.deadline < currentTime}>
                {editing && (
                    <Td colSpan={8}>
                        <LinearPreloader />
                    </Td>
                )}
                {!editing && (
                    <>
                        <Td style={{ display: 'flex' }}>
                            <Strip status={item.status} />
                            <CheckboxWrapper>
                                <CheckBox id={item.id} dept={dept} onChange={handleCheckTask} value={checkedValues[item.id]} />
                            </CheckboxWrapper>
                        </Td>
                        <Td>{item.number}</Td>
                        <Td onClick={() => { setIsClicked(!isClicked) }}>
                            <TaskText data-testid={`task-name-${item.id}`}>
                                {item.task}
                            </TaskText>
                        </Td>
                        <Td>
                            {/* <Status status={item.status}>{i18next.t(`task.boss.tasks.status.${item.status}`)}</Status> */}
                            <Select
                                id={`task-status-${item.id}`}
                                styles={customStyles()}
                                options={options}
                                defaultValue={options.find((o) => o.value === item.status)}
                                onChange={(option) => {
                                    dispatch(editTask({ ...item, status: option.value as TaskStatus }));
                                }}
                            />
                        </Td>
                        <Td>
                            <Priority priority={item.priority}>{i18next.t(`task.boss.tasks.priority.${item.priority}`)}</Priority>
                        </Td>
                        <Td>{item.performer}</Td>
                        <Td>
                            <Deadline isExpired={item.deadline < currentTime}>
                                {convertUnixTimestampToDateTime(item.deadline)}
                            </Deadline>
                        </Td>
                        <Td>{convertUnixTimestampToDateTime(item.lastChanged)}</Td>
                    </>
                )}
            </Tr>
            {isClicked && (
                <tr>
                    <Td colSpan={8}>
                        <Description>{item.description}</Description>
                    </Td>
                </tr>
            )}
        </>
    );
};

import styled from 'styled-components';
import _ from 'lodash';

import { TaskStatus, TaskPriority } from '../../../../../__data__/model/task';

export const Tr = styled.tr<{ isExpired: boolean }>`
    background-color: ${({ theme, isExpired }) => (isExpired ? theme.colors.danger.secondary : theme.colors.gray)};
    &:nth-child(odd) {
        background-color: ${({ theme, isExpired }) => (isExpired ? theme.colors.danger.secondary : theme.colors.gray.light)};
    }
`;

export const Td = styled.td`
    font-family: Montserrat;
    font-size: 12px;
    border: 1px solid ${({ theme }) => theme.colors.gray.dark};
    color: ${({ theme }) => theme.colors.black};
    height: 40px;
`;

export const Input = styled.input``;

export const CheckboxWrapper = styled.div`
    margin: 0 auto;
    justify-content: 'center';
    align-items: 'center';
`;

const statusToColor = {
    inProgress: 'orange',
    open: 'danger.main',
    needInfo: 'text.muted',
};

export const Strip = styled.div<{ status: TaskStatus }>`
    background-color: ${({ status, theme }) => _.get(theme.colors, statusToColor[status])};
    width: 3px;
    height: 100%;
`;

export const Status = styled.div<{ status: TaskStatus }>`
    margin: 0 auto;
    justify-content: center;
    align-items: center;
    border-radius: 10px;
    text-align: center;
    background-color: ${({ status, theme }) => _.get(theme.colors, statusToColor[status])};
    color: ${({ theme }) => theme.colors.white};
`;

export const TaskText = styled.div`
    text-align: left;
    padding-left: 10px;
    &:hover {
        cursor: pointer;
    }
`;

const priorityToColor = {
    high: 'orange',
    middle: 'black',
    low: 'action.main',
};

export const Priority = styled.div<{ priority: TaskPriority }>`
    color: ${({ theme, priority }) => _.get(theme.colors, priorityToColor[priority])};
`;

export const Deadline = styled.div<{ isExpired: boolean }>`
    color: ${({ theme, isExpired }) => (isExpired ? theme.colors.danger.main : theme.colors.black)};
`;

export const Description = styled.div`
    text-align: left;
    padding-left: 10px;
`;

import React from 'react';
import { useSelector } from 'react-redux';

import { Task } from '../../../../__data__/model/task';
import Table from '../table';
import * as selectors from '../../../../__data__/selectors';
import CircularLoader from '../../../../components/circular-loader';

import { Wrapper } from './style';

export const TableHolder = () => {
    const data = useSelector(selectors.tasks.data);
    const loading = useSelector(selectors.tasks.loading);
    if (loading) {
        return (
            <CircularLoader />
        );
    }
    return (
        <>
            {data.map((item) => (
                <Wrapper key={item.label}>
                    <div style={{ fontSize: '16pt' }}>{item.label}</div>
                    <Table data={item.data as Task[]} dept={item.label} />
                </Wrapper>
            ))}
        </>
    );
};

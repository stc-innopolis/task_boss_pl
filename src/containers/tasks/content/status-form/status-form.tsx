import i18next from 'i18next';
import React from 'react';

import { Form, FormRow, Label, Select, CreateButtonExtended } from './style';

export const StatusForm = () => (
    <Form>
        <FormRow>
            <Label>{i18next.t('task.boss.tasks.modal.edit.status.label')}</Label>
            <Select>
                <option>{i18next.t('task.boss.tasks.status.inProgress')}</option>
                <option>{i18next.t('task.boss.tasks.status.open')}</option>
                <option>{i18next.t('task.boss.tasks.status.needInfo')}</option>
            </Select>
        </FormRow>
        <CreateButtonExtended onClick={() => {}} variant="create">
            {i18next.t('task.boss.tasks.modal.edit.status.change')}
        </CreateButtonExtended>
    </Form>
);

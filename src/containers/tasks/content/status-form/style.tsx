import styled from 'styled-components';

import Button from '../../../../components/button';

export const Form = styled.form`
    display: flex;
    flex-direction: column;
`;

export const FormRow = styled.div`
    margin-top: 25px;
`;

export const Label = styled.label`
    display: block;
    font-size: 14px;
    color: ${(props) => props.theme.colors.text.common};
`;

export const Select = styled.select`
    width: 30%;
    height: 35px;
    border: 1px solid ${(props) => props.theme.colors.gray.dark};
    margin-top: 5px;
`;

export const CreateButtonExtended = styled(Button)`
    background-image: none;
    margin-top: 25px;
    text-indent: 5px;
`;

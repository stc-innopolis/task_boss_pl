import React, { useState, useEffect } from 'react';
import i18next from 'i18next';
import { useDispatch, useSelector } from 'react-redux';

import Button from '../../../components/button';
import Modal from '../../../components/modal';
import * as selectors from '../../../__data__/selectors';
import { getTasks } from '../../../__data__/actions/tasks';
import { deleteTasks } from '../../../__data__/actions/delete-tasks';
import { createTask } from '../../../__data__/actions/create-task';
import { editTask } from '../../../__data__/actions/edit-task';
import { actions as createTaskActions } from '../../../__data__/store/features/create-task';
import { actions as editTaskActions } from '../../../__data__/store/features/edit-task';

import TableHolder from './table-holder';
import { Wrapper, List, Item, Action } from './style';
import TaskForm from './task-form';
import StatusForm from './status-form';

type StandardLiProps = React.LiHTMLAttributes<HTMLUListElement>;

type ActionsProps = {
    onEdit: () => void,
    onChangeStatus: () => void,
    onDelete: () => void,
    editedTask: any,
    ids: any,
};

const Actions: React.FC<StandardLiProps & ActionsProps> = ({ onMouseLeave, onEdit, onChangeStatus, onDelete, editedTask, ids }) => (
    <List onMouseLeave={onMouseLeave}>
        {editedTask && (
            <Item>
                <Action data-testid="content-task-actions-edit" onClick={onEdit}>{i18next.t('task.boss.actions.tasks.edit')}</Action>
            </Item>
        )}
        <Item>
            <Action
                data-testid="content-task-actions-change"
                onClick={onChangeStatus}
            >
                {i18next.t('task.boss.actions.tasks.change')}
            </Action>
        </Item>
        {Object.keys(ids).length !== 0 && (
            <Item>
                <Action data-testid="content-task-actions-delete" onClick={onDelete}>{i18next.t('task.boss.actions.tasks.delete')}</Action>
            </Item>
        )}
    </List>
);

export const Content = () => {
    const [showModal, setShowModal] = useState(false);
    const [showActions, setShowActions] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);
    const [showStatusModal, setShowStatusModal] = useState(false);
    const editedTask = useSelector(selectors.editTask.editedTask);
    const checkedTasksIds = useSelector(selectors.checkTask.ids);
    const dept = useSelector(selectors.checkTask.dept);

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getTasks());
    }, [dispatch]);

    const handleActionsClick = () => {
        setShowActions(!showActions);
    };

    const handleActionsLeave = () => {
        setShowActions(false);
    };

    const handleEditTask = () => {
        if (editedTask) {
            setShowEditModal(!showEditModal);
        }
    };

    const handleEditStatus = () => {
        setShowStatusModal(!showStatusModal);
    };

    const handleOnDelete = () => {
        dispatch(deleteTasks(checkedTasksIds));
    };

    const createTaskOnClose = () => {
        setShowModal(!showModal);
        dispatch(createTaskActions.reset());
        dispatch(getTasks());
    };

    const createTaskOnSubmit = (data) => {
        data = {
            ...data,
            dept: data.dept.value,
            priority: data.priority.value,
        };
        dispatch(createTask(data));
        dispatch(createTaskActions.reset());
    };

    const createTaskLoading = useSelector(selectors.createTask.loading);
    const createTaskError = useSelector(selectors.createTask.error);
    const createTaskGot = useSelector(selectors.createTask.got);
    const createTaskSelectors = {
        taskLoading: createTaskLoading,
        taskError: createTaskError,
        taskGot: createTaskGot,
    };

    const editTaskOnClose = () => {
        setShowEditModal(!showEditModal);
    };

    const editTaskOnSubmit = (data) => {
        console.log(data);
        data = {
            ...data,
            dept: data.dept[0].value,
            priority: data.priority[0].value,
        };
        dispatch(editTask(data));
        dispatch(editTaskActions.reset());
    };

    const editTaskLoading = useSelector(selectors.editTask.loading);
    const editTaskError = useSelector(selectors.editTask.error);
    const editTaskGot = useSelector(selectors.editTask.loading);
    const editTaskSelectors = {
        taskLoading: editTaskLoading,
        taskError: editTaskError,
        taskGot: editTaskGot,
    };

    return (
        <Wrapper test-id="content-wrapper">
            {showModal && (
                <Modal onClose={() => setShowModal(!showModal)} title={i18next.t('task.boss.tasks.modal.new.task')}>
                    <TaskForm
                        onClose={createTaskOnClose}
                        onSubmit={createTaskOnSubmit}
                        taskSelectors={createTaskSelectors}
                        submitButtonLabel={i18next.t('task.boss.tasks.modal.new.task.add')}
                    />
                </Modal>
            )}
            {showActions && (
                <Actions
                    onMouseLeave={handleActionsLeave}
                    onEdit={handleEditTask}
                    onChangeStatus={handleEditStatus}
                    onDelete={handleOnDelete}
                    editedTask={editedTask}
                    ids={checkedTasksIds}
                />
            )}
            {showEditModal && (
                <Modal onClose={() => setShowEditModal(!showEditModal)} title={i18next.t('task.boss.tasks.modal.edit.task')}>
                    <TaskForm
                        onClose={editTaskOnClose}
                        onSubmit={editTaskOnSubmit}
                        taskSelectors={editTaskSelectors}
                        submitButtonLabel={i18next.t('task.boss.tasks.modal.edit.task.button.label')}
                        task={editedTask}
                        dept={dept}
                    />
                </Modal>
            )}
            {showStatusModal && (
                <Modal
                    onClose={() => setShowStatusModal(!showStatusModal)}
                    title={i18next.t('task.boss.tasks.modal.edit.status')}
                >
                    <StatusForm />
                </Modal>
            )}
            <Button
                data-testid="content-button-create"
                variant="create"
                onClick={() => setShowModal(!showModal)}
                style={{ marginLeft: '55px', marginTop: '10px' }}
            >
                {i18next.t('task.boss.buttons.create')}
            </Button>
            <Button
                data-testid="content-button-actions"
                variant="actions"
                onClick={handleActionsClick}
                style={{ marginLeft: '19px', marginTop: '10px' }}
            >
                {i18next.t('task.boss.buttons.actions')}
            </Button>
            <TableHolder />
        </Wrapper>
    );
};

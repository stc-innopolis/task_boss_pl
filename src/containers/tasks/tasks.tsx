import React from 'react';
import { useDispatch } from 'react-redux';
import { getFeatures } from '@ijl/cli';

import CommonPage from '../../components/common-page';
import { setSearch } from '../../__data__/actions/tasks';

import Content from './content';

const tasksSearchFeature = getFeatures('task-boss')['tasks.search'];

export const Tasks = () => {
    const dispatch = useDispatch();
    const onFilter = (value) => {
        dispatch(setSearch(value));
    };
    return (
        <CommonPage userName="Администратор" showSearch={tasksSearchFeature} onFilter={onFilter}>
            <Content />
        </CommonPage>
    );
};

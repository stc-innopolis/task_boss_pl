import React from 'react';
import { describe, it, expect } from '@jest/globals';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';

import NotFoundPage from '../../components/not_found_page';
import store from '../../__data__/store';
import { theme } from '../../app-theme';

describe('Not found page component', () => {
    it('render', () => {
        const { container } = render(
            <Provider store={store}>
                <StaticRouter location="/tasks">
                    <ThemeProvider theme={theme}>
                        <NotFoundPage />
                    </ThemeProvider>
                </StaticRouter>
            </Provider>,
        );
        expect(container).toMatchSnapshot();
    });
});

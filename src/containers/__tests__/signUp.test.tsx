import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import { describe, it, expect, beforeEach } from '@jest/globals';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import Mockadapter from 'axios-mock-adapter';

import axios from '../../utils/axios';
import store from '../../__data__/store';
import SignUpPage from '../sign-up';
import { theme } from '../../app-theme';
import { multipleRequest } from '../../utils/testutils';

describe('signUpPage', () => {
    let mockAxios;
    beforeEach(() => {
        mockAxios = new Mockadapter(axios);
    });

    it('rendering', async () => {
        const { container } = render(
            <Provider store={store}>
                <StaticRouter location="/sign-up">
                    <ThemeProvider theme={theme}>
                        <SignUpPage />
                    </ThemeProvider>
                </StaticRouter>
            </Provider>,
        );

        const loginInput = container.querySelector('#login');
        fireEvent.change(loginInput, { target: { value: '1234' } });
        const passwordInput = container.querySelector('#password');
        fireEvent.change(passwordInput, { target: { value: 'P@ssw0rd' } });
        const repeatPasswordInput = container.querySelector('#repeatPassword');
        fireEvent.change(repeatPasswordInput, { target: { value: 'P@ssw0rd' } });
        const mail = container.querySelector('#mail');
        fireEvent.change(mail, { target: { value: 'mail@mail.ru' } });
        fireEvent.click(container.querySelector('button[type="submit"]'));

        const response = [
            ['POST', '/sign-up', {}, 200, require('../../../stubs/api/json/sign-up/error.json')],
        ];

        await multipleRequest(mockAxios, response, true);

        expect(container).toMatchSnapshot();
    });
});

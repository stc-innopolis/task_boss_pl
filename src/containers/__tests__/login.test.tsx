import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import { describe, it, expect, beforeEach } from '@jest/globals';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import Mockadapter from 'axios-mock-adapter';

import axios from '../../utils/axios';
import store from '../../__data__/store';
import LoginPage from '../login';
import { theme } from '../../app-theme';
import { multipleRequest } from '../../utils/testutils';

describe('LoginPage', () => {
    let mockAxios;
    beforeEach(() => {
        mockAxios = new Mockadapter(axios);
    });

    it('rendering', async () => {
        const { container } = render(
            <Provider store={store}>
                <StaticRouter location="/login">
                    <ThemeProvider theme={theme}>
                        <LoginPage />
                    </ThemeProvider>
                </StaticRouter>
            </Provider>,
        );

        fireEvent.click(container.querySelector('button[type="submit"]'));
        const loginInput = container.querySelector('#login');
        fireEvent.change(loginInput, { target: { value: 'string' } });
        fireEvent.click(container.querySelector('button[type="submit"]'));
        expect(container).toMatchSnapshot();
        const passwordInput = container.querySelector('#password');
        fireEvent.change(passwordInput, { target: { value: 'string' } });
        fireEvent.click(container.querySelector('button[type="submit"]'));

        const response = [
            ['POST', '/login', {}, 200, require('../../../stubs/api/json/login/illegal-credentials.json')],
        ];

        await multipleRequest(mockAxios, response, true);
        expect(container).toMatchSnapshot();
    });
});

import { describe, it, expect } from '@jest/globals';

import { init } from '../depts/content/table/depts-table';

const dataset = [
    {
        id: '001',
        parentId: '0',
        name: 'Головной',
        qty: 56,
        responsible: {
            id: 1,
            family: 'Иванов',
            name: 'Иван',
            secondName: 'Иванович',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 12,
            newTasks: 54,
        },
        notes: '',
    },
    {
        id: '002',
        parentId: '0',
        name: 'Цех',
        qty: 52,
        responsible: {
            id: 2,
            family: 'Буйнов',
            name: 'Александр',
            secondName: 'Александрович',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 67,
            newTasks: 12,
        },
        notes: '',
    },
    {
        id: '003',
        parentId: '002',
        name: 'Токарка',
        qty: 6,
        responsible: {
            id: 3,
            family:
                'Петров',
            name: 'Иван',
            secondName:
                'Иванович',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 22,
            newTasks: 2,
        },
        notes: '1 этаж. красный сектор',
    },
    {
        id: '004',
        parentId: '002',
        name: 'Слесарка',
        qty: 34,
        responsible: {
            id: 3,
            family: 'Петров',
            name: 'Иван',
            secondName: 'Иванович',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 34,
            newTasks: 10,
        },
        notes: '1 этаж. синий сектор',
    },
    {
        id: '005',
        parentId: '002',
        name: 'Отрезной',
        qty: 12,
        responsible: {
            id: 4,
            family: 'Сидоров',
            name: 'Сергей',
            secondName: 'Викторович',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 11,
            newTasks: 0,
        },
        notes: '1 этаж. оранжевый сектор',
    },
    {
        id: '006',
        parentId: '0',
        name: 'Снабжение',
        qty: 1,
        responsible: {
            id: 5,
            family: 'Зыков',
            name: 'Александр',
            secondName: 'Геннадьевич',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 2,
            newTasks: 0,
        },
        notes: '',
    }];


const resultDataset = [
    {
        id: '001',
        parentId: '0',
        name: 'Головной',
        qty: 56,
        responsible: {
            id: 1,
            family: 'Иванов',
            name: 'Иван',
            secondName: 'Иванович',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 12,
            newTasks: 54,
        },
        notes: '',
        display: true,
        subdepts: [],
        expandIco: false,
        leaf: true,
        level: 1,
    },
    {
        id: '002',
        parentId: '0',
        name: 'Цех',
        qty: 52,
        responsible: {
            id: 2,
            family: 'Буйнов',
            name: 'Александр',
            secondName: 'Александрович',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 67,
            newTasks: 12,
        },
        notes: '',
        display: true,
        subdepts: [
            '003',
            '004',
            '005',
        ],
        expandIco: true,
        leaf: false,
        level: 1,
    },
    {
        id: '003',
        parentId: '002',
        name: 'Токарка',
        qty: 6,
        responsible: {
            id: 3,
            family: 'Петров',
            name: 'Иван',
            secondName: 'Иванович',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 22,
            newTasks: 2,
        },
        notes: '1 этаж. красный сектор',
        display: false,
        subdepts: [],
        expandIco: false,
        leaf: true,
        level: 2,
    },
    {
        id: '004',
        parentId: '002',
        name: 'Слесарка',
        qty: 34,
        responsible: {
            id: 3,
            family: 'Петров',
            name: 'Иван',
            secondName: 'Иванович',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 34,
            newTasks: 10,
        },
        notes: '1 этаж. синий сектор',
        display: false,
        subdepts: [],
        expandIco: false,
        leaf: true,
        level: 2,
    },
    {
        id: '005',
        parentId: '002',
        name: 'Отрезной',
        qty: 12,
        responsible: {
            id: 4,
            family: 'Сидоров',
            name: 'Сергей',
            secondName: 'Викторович',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 11,
            newTasks: 0,
        },
        notes: '1 этаж. оранжевый сектор',
        display: false,
        subdepts: [],
        expandIco: false,
        leaf: true,
        level: 2,
    },
    {
        id: '006',
        parentId: '0',
        name: 'Снабжение',
        qty: 1,
        responsible: {
            id: 5,
            family: 'Зыков',
            name: 'Александр',
            secondName: 'Геннадьевич',
            phone: '',
            email: '',
        },
        tasks: {
            inWork: 2,
            newTasks: 0,
        },
        notes: '',
        display: true,
        subdepts: [],
        expandIco: false,
        leaf: true,
        level: 1,
    },
];

describe('init', () => {
    it('check init method', () => {
        expect(init(dataset)).toStrictEqual(resultDataset);
    });
});

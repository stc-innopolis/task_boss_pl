import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import { describe, it, expect, beforeEach } from '@jest/globals';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import Mockadapter from 'axios-mock-adapter';

import { authAxios } from '../../utils/axios';
import store from '../../__data__/store';
import DeptsPage from '../depts';
import { theme } from '../../app-theme';
import { multipleRequest } from '../../utils/testutils';

describe('DeptsPage', () => {
    let mockAxios;
    beforeEach(() => {
        mockAxios = new Mockadapter(authAxios);
    });

    it('rendering', async () => {
        const { container } = render(
            <Provider store={store}>
                <StaticRouter location="/depts">
                    <ThemeProvider theme={theme}>
                        <DeptsPage />
                    </ThemeProvider>
                </StaticRouter>
            </Provider>,
        );

        expect(container).toMatchSnapshot();

        const response = [
            ['GET', '/depts', {}, 200, require('../../../stubs/api/json/depts/data2.json')],
        ];

        await multipleRequest(mockAxios, response, true);
        expect(container).toMatchSnapshot();
    });

    it('create dept', async () => {
        const { container, getAllByTestId, getByTestId } = render(
            <Provider store={store}>
                <StaticRouter location="/depts">
                    <ThemeProvider theme={theme}>
                        <DeptsPage />
                    </ThemeProvider>
                </StaticRouter>
            </Provider>,
        );

        const response = [
            ['GET', '/depts', {}, 200, require('../../../stubs/api/json/depts/data2.json')],
        ];

        await multipleRequest(mockAxios, response, true);
        mockAxios.reset();

        fireEvent.click(container.querySelector('#create-dept'));

        fireEvent.click(container.querySelector('button[data-test="next-btn"]'));

        const nameInput = container.querySelector('input[type="text"]');
        fireEvent.change(nameInput, { target: { value: 'new dept' } });
        fireEvent.click(container.querySelector('button[data-test="next-btn"]'));

        const resp = [
            ['GET', '/peoples', {}, 200, require('../../../stubs/api/json/depts/peoples/success.json')],
            ['GET', '/depts', {}, 200, require('../../../stubs/api/json/depts/data2.json')],
        ];

        await multipleRequest(mockAxios, resp, true);
        mockAxios.reset();

        fireEvent.click(container.querySelector('button[data-test="back-btn"]'));

        fireEvent.click(container.querySelector('button[data-test="next-btn"]'));

        fireEvent.click(container.querySelector('button[data-test="next-btn"]'));

        fireEvent.change(getAllByTestId('select')[0], { target: { value: '001' } });

        fireEvent.change(getAllByTestId('select')[1], { target: { value: 1 } });

        fireEvent.change(container.querySelector('input[data-test="mail-input"]'), { target: { value: 'my mail' } });

        fireEvent.click(container.querySelector('button[data-test="next-btn"]'));

        expect(container).toMatchSnapshot();

        fireEvent.change(container.querySelector('input[data-test="mail-input"]'), { target: { value: 'randomMail@gmail.com' } });

        fireEvent.click(container.querySelector('button[data-test="next-btn"]'));

        const res = [
            ['GET', '/peoples', {}, 200, require('../../../stubs/api/json/depts/peoples/success.json')],
        ];

        await multipleRequest(mockAxios, res, true);
        mockAxios.reset();

        fireEvent.change(getByTestId('select'), { target: { value: 1 } });
        fireEvent.click(container.querySelector('#add-button'));

        fireEvent.click(container.querySelector('button[data-test="next-btn"]'));

        expect(container).toMatchSnapshot();
    });

    it('tree table testing', async () => {
        const { container } = render(
            <Provider store={store}>
                <StaticRouter location="/depts">
                    <ThemeProvider theme={theme}>
                        <DeptsPage />
                    </ThemeProvider>
                </StaticRouter>
            </Provider>,
        );

        const response = [
            ['GET', '/depts', {}, 200, require('../../../stubs/api/json/depts/data2.json')],
        ];

        await multipleRequest(mockAxios, response, true);
        mockAxios.reset();

        expect(container.querySelector('table')).toMatchSnapshot();

        fireEvent.click(container.querySelector('table span'));

        expect(container.querySelector('table')).toMatchSnapshot();
    });
});

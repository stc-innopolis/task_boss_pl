import React from 'react';
import { describe, it, expect, beforeEach } from '@jest/globals';
import MockAdapter from 'axios-mock-adapter';
import { render, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import '@testing-library/jest-dom';

import { authAxios as axios } from '../../utils/axios';
import store from '../../__data__/store';
import { theme } from '../../app-theme';
import TasksPage from '../tasks';
import { multipleRequest } from '../../utils/testutils';

describe('TasksPage', () => {
    let mockAxios;
    beforeEach(() => {
        mockAxios = new MockAdapter(axios);
        Date.prototype.getHours = function getHours() { return 42 };
    });

    it('render', async () => {
        const { container } = render(
            <Provider store={store}>
                <StaticRouter location="/tasks">
                    <ThemeProvider theme={theme}>
                        <TasksPage />
                    </ThemeProvider>
                </StaticRouter>
            </Provider>,
        );

        expect(container).toMatchSnapshot();

        const response = [
            ['GET', '/tasks', {}, 200, require('../../../stubs/api/json/tasks/success.json')],
        ];
        await multipleRequest(mockAxios, response, true);

        expect(container).toMatchSnapshot();
    });

    it('search/filter tasks', async () => {
        const { container } = render(
            <Provider store={store}>
                <StaticRouter location="/tasks">
                    <ThemeProvider theme={theme}>
                        <TasksPage />
                    </ThemeProvider>
                </StaticRouter>
            </Provider>,
        );

        const response = [
            ['GET', '/tasks', {}, 200, require('../../../stubs/api/json/tasks/success.json')],
        ];
        await multipleRequest(mockAxios, response, true);

        expect(container).toMatchSnapshot();

        fireEvent.change(container.querySelector('input[data-testid="header-search-bar-input"]'), { target: { value: 'Установить' } });
        fireEvent.click(container.querySelector('button[data-testid="header-search-bar-button"]'));

        expect(container).toMatchSnapshot();
    });

    it('actions button', async () => {
        const { container } = render(
            <Provider store={store}>
                <StaticRouter location="/tasks">
                    <ThemeProvider theme={theme}>
                        <TasksPage />
                    </ThemeProvider>
                </StaticRouter>
            </Provider>,
        );

        const response = [
            ['GET', '/tasks', {}, 200, require('../../../stubs/api/json/tasks/success.json')],
        ];
        await multipleRequest(mockAxios, response, true);

        fireEvent.click(container.querySelector('#task-checkbox-1'));
        fireEvent.click(container.querySelector('button[data-testid="content-button-actions"]'));
        fireEvent.click(container.querySelector('button[data-testid="content-task-actions-delete"]'));

        fireEvent.click(container.querySelector('button[data-testid="content-task-actions-edit"]'));
        fireEvent.click(container.querySelector('button[data-testid="task-form-submit"]'));

        expect(container).toMatchSnapshot();
    });

    it('create new task', async () => {
        const { container } = render(
            <Provider store={store}>
                <StaticRouter location="/tasks">
                    <ThemeProvider theme={theme}>
                        <TasksPage />
                    </ThemeProvider>
                </StaticRouter>
            </Provider>,
        );

        const response = [
            ['GET', '/tasks', {}, 200, require('../../../stubs/api/json/tasks/success.json')],
        ];
        await multipleRequest(mockAxios, response, true);
        mockAxios.reset();

        fireEvent.click(container.querySelector('button[data-testid="content-button-create"]'));
        fireEvent.click(container.querySelector('a[data-testid="modal-button-close"]'));

        fireEvent.click(container.querySelector('button[data-testid="content-button-create"]'));

        const responseDepts = [
            ['GET', '/depts', {}, 200, require('../../../stubs/api/json/depts/data2.json')],
        ];
        await multipleRequest(mockAxios, responseDepts, true);
        mockAxios.reset();

        fireEvent.change(container.querySelector('input[data-testid="task-form-task"]'),
            { target: { value: 'Установить Microsoft Office' } });
        fireEvent.change(container.querySelector('input[data-testid="task-form-performer"]'),
            { target: { value: 'Иванов Иван Иванович' } });
        fireEvent.change(container.querySelector('#task-form-dept'), { target: { value: 'Головной' } });
        fireEvent.change(container.querySelector('#task-form-priority'), { target: { value: 'Высокий' } });

        fireEvent.click(container.querySelector('button[data-testid="task-form-submit"]'));

        fireEvent.change(container.querySelector('input[data-testid="task-form-deadline"]'), { target: { value: '2021-10-31' } });
        fireEvent.change(container.querySelector('textarea[data-testid="task-form-description"]'), { target: { value: 'Описание' } });

        fireEvent.click(container.querySelector('button[data-testid="task-form-submit"]'));

        await multipleRequest(mockAxios, response, true);
        mockAxios.reset();

        expect(container).toMatchSnapshot();
    });

    it('table row actions', async () => {
        const { container } = render(
            <Provider store={store}>
                <StaticRouter location="/tasks">
                    <ThemeProvider theme={theme}>
                        <TasksPage />
                    </ThemeProvider>
                </StaticRouter>
            </Provider>,
        );

        const response = [
            ['GET', '/tasks', {}, 200, require('../../../stubs/api/json/tasks/success.json')],
        ];
        await multipleRequest(mockAxios, response, true);

        fireEvent.click(container.querySelector('div[data-testid="task-name-1"]'));
        fireEvent.change(container.querySelector('#task-status-1'), { target: { value: 'open' } });

        expect(container).toMatchSnapshot();
    });
});

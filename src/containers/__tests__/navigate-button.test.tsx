import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { describe, it, expect } from '@jest/globals';
import { ThemeProvider } from 'styled-components';

import NavigateButton from '../depts/content/buttons/navigate-button';
import { theme } from '../../app-theme';

describe('NavigateButton component', () => {
    it('navigate button rendering', async () => {
        const text = 'Создать';
        render(
            <ThemeProvider theme={theme}>
                <NavigateButton>{text}</NavigateButton>
            </ThemeProvider>,
        );
        expect(await screen.findByText(text)).toMatchSnapshot();
    });
});

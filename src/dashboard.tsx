import React, { useMemo } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { URLs } from './__data__/urls';
import Login from './containers/login';
import SignUp from './containers/sign-up';
import Depts from './containers/depts';
import Tasks from './containers/tasks';
import NotFoundPage from './components/not_found_page';

const useAuth = () => {
    const token = useMemo(() => localStorage.getItem('task-boss-token'), []);

    return { token };
};

const AuthorizedRoute = ({ children, ...rest }) => {
    const auth = useAuth();

    return (
        <Route
            {...rest}
            render={({ location }) => (auth.token ? (
                children
            ) : (
                <Redirect
                    to={{
                        pathname: URLs.login.url,
                        state: { from: location },
                    }}
                />
            ))}
        />
    );
};

const Dashboard = () => (
    <Switch>
        <Route exact path={URLs.root.url}>
            <Redirect to={URLs.login.url} />
        </Route>
        <Route path={URLs.login.url}>
            <Login />
        </Route>
        <Route path={URLs.signUp.url}>
            <SignUp />
        </Route>
        <AuthorizedRoute path={URLs.depts.url}>
            <Depts />
        </AuthorizedRoute>
        <AuthorizedRoute path={URLs.tasks.url}>
            <Tasks />
        </AuthorizedRoute>
        <Route>
            <NotFoundPage />
        </Route>
    </Switch>
);

export default Dashboard;

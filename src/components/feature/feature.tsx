import React, { Suspense } from 'react';

import { getFeatures } from '@ijl/cli';
import ErrorBoundary from '../error-boundary';

const features = getFeatures('task-boss');

export const Feature = ({ fallback, featureName, children }) => {
    const isFeatureOn = features[featureName];

    if (!isFeatureOn) return null;

    return (
        <ErrorBoundary>
            <Suspense fallback={fallback}>{children}</Suspense>
        </ErrorBoundary>
    );
};

Feature.defaultProps = {
    fallback: '...loading',
};

import styled from 'styled-components';

import { downArrow } from '../../assets';

export const Button = styled.button`
    font-family: Montserrat;
    width: 122px;
    height: 34px;
    text-indent: -18px;
    background: url(${downArrow}) no-repeat scroll 7px 7px;
    background-position: bottom 6px right 14px;
    background-size: 17px;
    background-color: ${(props) => props.theme.colors.white};
    border-radius: 30px;
    border: 2px solid ${(props) => props.theme.colors.action.main};
    color: ${(props) => props.theme.colors.action.main};
    &:hover {
        border-color: ${(props) => props.theme.colors.action.mainHover};
    }
`;

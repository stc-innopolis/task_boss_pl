import React from 'react';

import { Button } from './style';

const ActionButton = ({ className }) => <Button className={className}>Действия</Button>;

export default ActionButton;

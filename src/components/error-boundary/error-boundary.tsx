import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.span`
    color: ${(props) => props.theme.colors.danger.main};
`;

type ErrorBoundaryState = {
    error: string;
};

type ErrorBoundaryProps = {
    children: React.ReactElement;
};

export default class ErrorBoundary extends React.Component<ErrorBoundaryProps, ErrorBoundaryState> {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
        };
    }

    static getDerivedStateFromError(error) {
        return { error: error.message };
    }

    render() {
        const { children } = this.props;
        const { error } = this.state;

        if (error) {
            return <Wrapper>{error}</Wrapper>;
        }

        return children;
    }
}

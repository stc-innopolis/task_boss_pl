import React from 'react';
import Select from 'react-select';

export const SelectComponent = ({ ...rest }) => (
    <Select
        {...rest}
    />
);


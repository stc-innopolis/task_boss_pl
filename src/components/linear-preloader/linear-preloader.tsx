import React from 'react';

import { Progress, Indeterminate } from './style';

export const LinearPreloader = () => (
    <Progress>
        <Indeterminate />
    </Progress>
);

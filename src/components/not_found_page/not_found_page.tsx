import React from 'react';
import { Link } from 'react-router-dom';

import { notFound } from '../../assets';
import { Wrapper, NotFoundImg, ErorrText, Text } from './style';
import { URLs } from '../../__data__/urls';

export const NotFoundPage = () => (
    <Wrapper>
        <NotFoundImg src={notFound} alt="Страница не найдена" />
        <ErorrText>Not founD</ErorrText>
        <Text>
            Вернуться на
            {' '}
            <Link style={{ textDecoration: 'none' }} to={URLs.root.url}>
                <Text>главную</Text>
            </Link>
        </Text>
    </Wrapper>
);

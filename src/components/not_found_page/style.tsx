import styled from 'styled-components';

export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

export const ErorrText = styled.div`
    font-size: 64px;
    font-weight: bold;
    padding-top: 50px;
    color: ${(props) => props.theme.colors.orange};
`;

export const Text = styled.span`
    font-size: 24px;
    padding-top: 21px;
    text-decoration: none;
`;

export const NotFoundImg = styled.img`
    margin-left: auto;
    margin-right: auto;
`;

import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { describe, it, expect } from '@jest/globals';
import { ThemeProvider } from 'styled-components';

import Button from '../button';
import { theme } from '../../app-theme';

describe('Button component', () => {
    it('rendering', async () => {
        const text = 'some text';
        render(
            <ThemeProvider theme={theme}>
                <Button variant="create">{text}</Button>
            </ThemeProvider>,
        );
        expect(await screen.findByText(text)).toMatchSnapshot();
    });
});

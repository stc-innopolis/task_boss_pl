import styled from 'styled-components';

export const Button = styled.button`
    background-color: ${(props) => props.theme.colors.action.secondary};
    width: 400px;
    color: ${(props) => props.theme.colors.white};
    text-align: center;
    display: block;
    font-size: 18px;
    border-radius: 10px;
    padding-top: 12px;
    padding-bottom: 12px;
`;

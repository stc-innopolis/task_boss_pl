import React from 'react';

import { Button } from './styled';

export const NextButton = ({ type }) => <Button type={type} />;

NextButton.defaultProps = {
    type: 'text',
};

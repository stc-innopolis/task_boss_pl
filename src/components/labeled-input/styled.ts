import styled from 'styled-components';

export type FormInputProps = {
    error?: string;
};

export const ContainerInput = styled.div`
    flex-direction: column;
    display: flex;
    justify-content: space-around;
    text-align: start;
    font-size: 25px;
    font-weight: 900;
    width: 400px;
    margin-left: auto;
    margin-right: auto;
    padding-top: 12px;
    padding-bottom: 12px;
    position: relative;
`;

export const Label = styled.label``;

export const Input = styled.input<FormInputProps>`
    height: 35px;
    width: 400px;
    font-size: 20px;
    font-family: 'Roboto';
    border-color: ${(props) => (props.error ? props.theme.colors.danger.main : props.theme.colors.action.secondary)};
    border-width: 3px;
`;

export const Eye = styled.img`
    
    height: 30px;
    width: 30px;
    position: absolute;
    top: 20px;
    right: -4px;
    
`;

export const MessageError = styled.div`
    font-family: 'Roboto';
    font-style: normal;
    color: ${(props) => props.theme.colors.danger.main};
    font-size: 18px;
`;

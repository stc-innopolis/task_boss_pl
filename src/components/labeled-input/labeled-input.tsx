import React, { ForwardedRef, useState, useMemo } from 'react';

import { closedeye, openedeye } from '../../assets';
import { ContainerInput, Label, Input, FormInputProps, Eye, MessageError } from './styled';

type LabeledInputProps = FormInputProps &
    Omit<React.InputHTMLAttributes<HTMLInputElement>, 'ref'> & {
        id: string;
        inputRef?: ForwardedRef<HTMLInputElement>;
    };

export const LabeledInput: React.FC<LabeledInputProps> = ({ id, type, error, ...values }) => {
    const [isOpenedEye, setIsOpenedEye] = useState(true);
    const inputType = useMemo(() => {
        if (type === 'password') {
            return isOpenedEye ? 'password' : 'text';
        }
        return type;
    }, [isOpenedEye, type]);

    return (
        <ContainerInput>
            <Label htmlFor={id} />
            <Input type={inputType} id={id} error={error} {...values} />
            {type === 'password' && (
                <Eye onClick={() => setIsOpenedEye((e) => !e)} src={isOpenedEye ? openedeye : closedeye} />
            )}
            {error && <MessageError>{error}</MessageError>}
        </ContainerInput>
    );
};

LabeledInput.defaultProps = { type: 'text', inputRef: null };

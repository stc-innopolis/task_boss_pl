import styled from 'styled-components';

import { plus, downArrow } from '../../assets';

export const NextButton = styled.button`
    background-color: ${(props) => props.theme.colors.action.secondary};
    width: 400px;
    color: ${(props) => props.theme.colors.white};
    text-align: center;
    display: block;
    font-size: 18px;
    border-radius: 10px;
    padding-top: 12px;
    padding-bottom: 12px;
`;

export const CreateButton = styled.button`
    font-family: Montserrat;
    width: 122px;
    height: 34px;
    text-indent: 10px;
    background: url(${plus}) no-repeat scroll 7px 7px;
    background-position: bottom 8px left 16px;
    background-size: 16px;
    background-color: ${(props) => props.theme.colors.action.main};
    border-radius: 30px;
    border: 1px solid ${(props) => props.theme.colors.action.main};
    color: ${(props) => props.theme.colors.white};
    &:hover {
        background-color: ${(props) => props.theme.colors.action.mainHover};
    }
`;

export const ActionsButton = styled.button`
    font-family: Montserrat;
    width: 122px;
    height: 34px;
    text-indent: -18px;
    background: url(${downArrow}) no-repeat scroll 7px 7px;
    background-position: bottom 6px right 14px;
    background-size: 17px;
    background-color: ${(props) => props.theme.colors.white};
    border-radius: 30px;
    border: 2px solid ${(props) => props.theme.colors.action.main};
    color: ${(props) => props.theme.colors.action.main};
    &:hover {
        border-color: ${(props) => props.theme.colors.action.mainHover};
    }
`;

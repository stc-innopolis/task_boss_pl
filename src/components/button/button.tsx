import React from 'react';

import { NextButton, CreateButton, ActionsButton } from './style';

const variants = {
    next: NextButton,
    create: CreateButton,
    actions: ActionsButton,
};

type ButtonProps = {
    variant: keyof typeof variants;
};

type StandardButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement>;

export const Button: React.FC<ButtonProps & StandardButtonProps> = ({ variant, children, ...rest }) => {
    const MyButton = variants[variant];
    return <MyButton {...rest}>{children}</MyButton>;
};

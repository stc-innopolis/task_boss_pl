import React from 'react';

import { StyledModal, StyledModalContent, StyledHeader, StyledP, StyledExitButton, StyledMain } from './style';

const Modal = ({ title, onClose, children }) => (
    <div>
        <StyledModal>
            <StyledModalContent>
                <StyledHeader>
                    <StyledP>{title}</StyledP>
                    <StyledExitButton data-testid="modal-button-close" onClick={onClose} title="Закрыть" />
                </StyledHeader>
                <StyledMain>{children}</StyledMain>
            </StyledModalContent>
        </StyledModal>
    </div>
);

export default Modal;

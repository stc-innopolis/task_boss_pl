import styled from 'styled-components';

import { close } from '../../assets';

export const StyledModal = styled.div`
    font-size: 20px;
    font-weight: 100;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    z-index: 9999;
    position: fixed;
    background-color: ${(props) => props.theme.colors.modal.background};
    padding-top: 2%;
`;

export const StyledModalContent = styled.div`
    width: 95vw;
    max-width: 865px;
    height: 90vh;
    max-height: 761px;
    position: relative;
    overflow: hidden;
    border-radius: 4px;
    margin: 0 auto;
    background-color: ${(props) => props.theme.colors.gray.dark};
    display: flex;
    flex-direction: column;
`;

export const StyledHeader = styled.header`
    min-height: 40px;
    height: 40px;
    color: ${(props) => props.theme.colors.white};
    background-color: ${(props) => props.theme.colors.orange};
    display: grid;
    padding-left: 14px;
    align-items: center;
    grid-template-columns: auto 60px;
`;

export const StyledExitButton = styled.a`
    font-family: cursive;
    color: ${(props) => props.theme.colors.white};
    background: url(${close}) no-repeat scroll 7px 7px;
    background-position: center;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    width: 100%;
    font-size: 20px;
    opacity: 1;
    cursor: pointer;
    &:hover {
        opacity: 1;
    }
`;

export const StyledP = styled.p`
    margin: 0;
    text-align: left;
`;

export const StyledMain = styled.div`
    flex: 1;
    text-align: left;
    overflow: auto;
    padding: 14px;
`;

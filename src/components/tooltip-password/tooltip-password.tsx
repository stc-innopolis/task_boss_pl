import React, { useEffect, useState } from 'react';

import { Tooltip, LockCrossExt, LockCheckmarkExt, LockIconExt, WrapperRow, Text } from './styled';

const useRegex = (value) => {
    const [checkRegex, setCheckRegex] = useState({
        lenghtWord: false,
        registrWord: false,
        specChar: false,
    });

    useEffect(() => {
        setCheckRegex({
            ...checkRegex,
            lenghtWord: value.match('.{8,}'),
            registrWord: value.match('(?=.*?[A-Z])(?=.*?[a-z])'),
            specChar: value.match('(?=.*?[#?!@$%^&*-])'),
        });
    }, [value]);
    return {
        ...checkRegex,
    };
};

export const TooltipPassword = (props) => {
    const { passValue } = props;
    const { lenghtWord, specChar, registrWord } = useRegex(passValue);
    return (
        <Tooltip visible={props}>
            <WrapperRow>
                <LockIconExt $danger={lenghtWord && specChar && registrWord} />
                <Text $danger={lenghtWord && specChar && registrWord} />
            </WrapperRow>

            <WrapperRow>
                {lenghtWord ? <LockCheckmarkExt /> : <LockCrossExt />}
                <Text $danger={lenghtWord}>Длина пароля не менее 8 символов</Text>
            </WrapperRow>

            <WrapperRow>
                {specChar ? <LockCheckmarkExt /> : <LockCrossExt />}
                <Text $danger={specChar}>Пароль должен содержать спецсимволы !@#$%^&*</Text>
            </WrapperRow>

            <WrapperRow>
                {registrWord ? <LockCheckmarkExt /> : <LockCrossExt />}
                <Text $danger={registrWord}>Пароль должен содержать буквы верхнего и нижнего регистра</Text>
            </WrapperRow>
        </Tooltip>
    );
};

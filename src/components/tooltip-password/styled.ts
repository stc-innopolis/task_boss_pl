import styled from 'styled-components';
import { LockCross, LockCheckmark, LockIcon } from '../../assets';

export type FormInputProps = {
    $danger?: boolean;
    visible?: boolean;
};

export const Tooltip = styled.div<FormInputProps>`
    flex-direction: column;
    display: flex;
    min-height: 120px;
    border: 2px solid ${(props) => props.theme.colors.danger.main};
    border-radius: 11px;
    visibility: ${(props) => (props.visible ? 'visible' : 'hidden')};
`;

export const Img = styled.img`
    width: 50%;
    height: 30%;
`;

export const WrapperRow = styled.div`
    flex-direction: row;
    display: flex;
`;

export const LockCrossExt = styled(LockCross)`
    padding-left: 45px;
    padding-right: 0px;
    width: 12px;
    height: 23px;
`;
export const LockCheckmarkExt = styled(LockCheckmark)`
    padding-left: 45px;
    padding-right: 0px;
    width: 18px;
    height: 30px;
`;
export const LockIconExt = styled(LockIcon)<FormInputProps>`
    padding-top: 5px;
    padding-left: 5px;
    padding-right: 0px;
    color: ${(props) => (props.$danger ? props.theme.colors.action.main : props.theme.colors.danger.main)};
`;

export const Text = styled.span<FormInputProps>`
    padding-left: 15px;
    font-size: 14px;
    font-family: 'Roboto';
    font-style: normal;
    text-align: left;
    color: ${(props) => (props.$danger ? props.theme.colors.action.main : props.theme.colors.danger.main)};
`;

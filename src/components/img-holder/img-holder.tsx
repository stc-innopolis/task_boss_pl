import React from 'react';
import i18next from 'i18next';

import { Wrapper, Img, Description } from './style';

export type ImgHolderProps = {
    imgSrc: string;
    description?: string;
};

export const ImgHolder: React.FC<ImgHolderProps> = ({ imgSrc, description }) => (
    <Wrapper>
        <Img src={imgSrc} />
        {description && <Description>{i18next.t(`task.boss.sidebar.${description}`)}</Description>}
    </Wrapper>
);

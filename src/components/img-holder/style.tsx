import styled from 'styled-components';

export const Wrapper = styled.div``;

export const Img = styled.img`
    width: 100%;
    height: 100%;
    object-fit: contain;
`;

export const Description = styled.div`
    text-align: center;
    color: ${({ theme }) => theme.colors.white};
`;

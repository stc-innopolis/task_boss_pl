import React, { useCallback, useState } from 'react';
import i18next from 'i18next';

import { user, searchIcon } from '../../../assets';

import { Wrapper, SearchBarInput, Container, Span, Avatar, Form, Button } from './style';

const SearchBar = ({ placeHolder, onFilter }) => {
    const [value, setValue] = useState('');
    const handleInputChange = useCallback(
        (event) => {
            setValue(event.target.value);
        },
        [setValue],
    );
    const handleSubmit = useCallback(
        (event) => {
            event.preventDefault();
            onFilter(value);
        },
        [onFilter, value],
    );
    return (
        <Form onSubmit={handleSubmit}>
            <SearchBarInput data-testid="header-search-bar-input" placeholder={placeHolder} onChange={handleInputChange} />
            <Button data-testid="header-search-bar-button" title="Поиск" type="submit">
                <img src={searchIcon} width="100%" alt="Иконка лупы" />
            </Button>
        </Form>
    );
};

SearchBar.defaultProps = {
    placeHolder: 'Поиск...',
};

export const Header = ({ userName, showSearch, onFilter }) => {
    const handleExit = () => {
        localStorage.clear();
        window.location.reload();
    };

    return (
        <Wrapper>
            {showSearch && <SearchBar onFilter={onFilter} />}
            <Container>
                <Span>{userName}</Span>
                <Avatar src={user} />
                <Span onClick={handleExit}>{i18next.t('task.boss.header.exit')}</Span>
            </Container>
        </Wrapper>
    );
};


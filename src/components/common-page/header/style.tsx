import styled from 'styled-components';

export const Wrapper = styled.div`
    display: flex;
    position: fixed;
    margin-left: 120px;
    height: 70px;
    width: calc(100% - 120px);
    background-color: ${({ theme }) => theme.colors.gray.dark};
`;

export const SearchBarInput = styled.input`
    margin-left: 55px;
    margin-top: 20px;
    width: 100%;
    height: 30px;
    border-radius: 30px;
    border: 1px solid ${({ theme }) => theme.colors.gray.dark};
    text-indent: 10px;
    background-position: bottom 5px right 14px;
    background-size: 20px;
    background-color: ${({ theme }) => theme.colors.white};
    &:focus {
        outline: none;
    }
    @media (max-width: 1200px) {
        display: none;
    }
`;

export const Container = styled.div`
    margin-left: auto;
    height: 100%;
`;

export const Avatar = styled.img`
    height: 60px;
    margin-right: 10px;
    margin-top: 5px;
    border-radius: 50%;
`;

export const Span = styled.span`
    display: inline-block;
    cursor: pointer;
    line-height: 70px;
    font-family: Montserrat;
    margin-right: 11px;
    vertical-align: top;
`;

export const Form = styled.form`
    position: relative;
    width: 50%;
`;

export const Button = styled.button`
    position: absolute;
    width: 40px;
    height: 40px;
    background: transparent;
    border: none;
    right: -54px;
    top: 19px;
`;

import React from 'react';

import { URLs } from '../../__data__/urls';
import { tasks, depts } from '../../assets';

import Sidebar from './sidebar';
import Header from './header';

const sidebarItems = [];
sidebarItems.push(
    {
        url: URLs.tasks.url,
        imgSrc: tasks,
        description: 'tasks',
    },
    {
        url: URLs.depts.url,
        imgSrc: depts,
        description: 'depts',
    },
);

type CommonPageProps = {
    userName: string;
    showSearch?: boolean;
    onFilter?: (value: string) => void;
};

export const CommonPage: React.FC<CommonPageProps> = ({ userName, showSearch, onFilter, children }) => (
    <div>
        <Sidebar>
            <Sidebar.Item {...sidebarItems[0]} />
            <Sidebar.Item {...sidebarItems[1]} />
        </Sidebar>
        <Header userName={userName} showSearch={showSearch} onFilter={onFilter} />
        {children}
    </div>
);

CommonPage.defaultProps = {
    showSearch: true,
    onFilter: () => {},
};

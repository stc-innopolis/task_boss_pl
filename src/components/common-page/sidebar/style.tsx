import styled from 'styled-components';

export const Wrapper = styled.div`
    position: fixed;
    width: 120px;
    height: 100vh;
    background-color: ${({ theme }) => theme.colors.action.secondary};
`;

export const Logo = styled.div`
    margin-top: 20px;
`;

export const Items = styled.div`
    margin-top: 50px;
`;

export const ItemHolder = styled.div`
    margin-top: 30px;
    padding: 0 35px;
    font-size: 12pt;
    &:hover {
        background-color: ${({ theme }) => theme.colors.action.secondaryHover};
    }
`;

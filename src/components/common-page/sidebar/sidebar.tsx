import React from 'react';
import { Link } from 'react-router-dom';

import { Logo as LogoImg } from '../../../assets';
import { URLs } from '../../../__data__/urls';
import ImgHolder, { ImgHolderProps } from '../../img-holder';

import { Wrapper, Logo, Items, ItemHolder } from './style';

type ItemProps = ImgHolderProps & {
    url: string;
};

const Item: React.FC<ItemProps> = ({ url, imgSrc, description }) => (
    <Link to={url}>
        <ItemHolder>
            <ImgHolder imgSrc={imgSrc} description={description} />
        </ItemHolder>
    </Link>
);

export const Sidebar: React.FC<{}> & { Item: typeof Item } = ({ children }) => (
    <Wrapper>
        <Logo>
            <Link to={URLs.root.url}>
                <ImgHolder imgSrc={LogoImg} />
            </Link>
        </Logo>
        <Items>{children}</Items>
    </Wrapper>
);

Sidebar.Item = Item;

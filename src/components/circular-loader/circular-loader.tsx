import React from 'react';

import {
    Wrapper,
    PreloaderWrapper,
    SpinnerLayer,
    CircleClipperLeft,
    CircleClipperRight,
    CircleLeft,
    CircleRight,
    GapPatch,
    CircleGap,
} from './style';

export const CircularLoader = () => (
    <Wrapper>
        <PreloaderWrapper>
            <SpinnerLayer>
                <CircleClipperLeft>
                    <CircleLeft />
                </CircleClipperLeft>
                <GapPatch>
                    <CircleGap />
                </GapPatch>
                <CircleClipperRight>
                    <CircleRight />
                </CircleClipperRight>
            </SpinnerLayer>
        </PreloaderWrapper>
    </Wrapper>
);

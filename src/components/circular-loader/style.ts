import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 100%;
    margin-left: auto;
    margin-top: 20%;
    left: auto;
    right: auto;
    float: left;
    box-sizing: border-box;
    padding: 0 0.75rem;
    min-height: 1px;
    text-align: center;
    vertical-align: middle;
`;

export const PreloaderWrapper = styled.div`
    display: inline-block;
    justify-content: center;
    width: 64px;
    height: 64px;
    animation: container-rotate 1568ms linear infinite;
    @keyframes container-rotate {
        100% {
            transform:rotate(360deg);
        }
    };
`;

export const SpinnerLayer = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;
    border-color: ${({ theme }) => theme.colors.action.main};
    opacity: 1;
    animation: fill-unfill-rotate 5332ms cubic-bezier(0.4, 0, 0.2, 1) infinite both;
    @keyframes fill-unfill-rotate {
        12.5% {
            transform: rotate(135deg);
        }
        25% {
            transform: rotate(270deg);
        }
        37.5% {
            transform: rotate(405deg);
        }
        50% {
            transform: rotate(540deg);
        }
        62.5% {
            transform: rotate(675deg);
        }
        75% {
            transform: rotate(810deg);
        }
        87.5% {
            transform: rotate(945deg);
        }
        100% {
            transform: rotate(1080deg);
        }
    }
`;

const CircleClipper = styled.div`
    display: inline-block;
    position: relative;
    width: 50%;
    height: 100%;
    overflow: hidden;
    border-color: inherit;
`;

export const CircleClipperLeft = styled(CircleClipper)`
    float: left !important;
`;

export const CircleClipperRight = styled(CircleClipper)`
    float: left !important;
`;

export const GapPatch = styled.div`
    position: absolute;
    top: 0;
    left: 45%;
    width: 10%;
    height: 100%;
    overflow: hidden;
    border-color: inherit;
`;

const Circle = styled.div`
    width: 200%;
    height: 100%;
    border-width: 3px;
    border-style: solid;
    border-color: inherit;
    border-bottom-color: transparent !important;
    border-radius: 50%;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
`;

export const CircleLeft = styled(Circle)`
    left: 0;
    border-right-color: transparent !important;
    transform: rotate(129deg);
    animation: left-spin 1333ms cubic-bezier(0.4, 0, 0.2, 1) infinite both;
    @keyframes left-spin {
        0% {
            transform: rotate(130deg);
        }
        50% {
            transform: rotate(-5deg);
        }
        100% {
            transform: rotate(130deg);
        }
    }
`;

export const CircleRight = styled(Circle)`
    left: -100%;
    border-left-color: transparent !important;
    transform: rotate(-129deg);
    animation: right-spin 1333ms cubic-bezier(0.4, 0, 0.2, 1) infinite both;
    @keyframes right-spin {
        0% {
            transform: rotate(-130deg);
        }
        50% {
            transform: rotate(5deg);
        }
        100% {
            transform: rotate(-130deg);
        }
    }
`;

export const CircleGap = styled.div`
    border-radius: 50%;
    width: 1000%;
    left: -450%;
`;

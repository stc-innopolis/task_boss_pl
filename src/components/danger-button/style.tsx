import styled from 'styled-components';

export const Button = styled.button`
    font-family: Montserrat;
    cursor: pointer;
    width: 122px;
    height: 34px;
    background-color: ${(props) => props.theme.colors.danger.main};
    border-radius: 30px;
    border: 1px solid ${(props) => props.theme.colors.danger.main};
    color: ${(props) => props.theme.colors.white};
    &:hover {
        background-color: ${(props) => props.theme.colors.danger.main};
    }
`;

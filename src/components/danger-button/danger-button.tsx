import React from 'react';

import { Button } from './style';

const DangerButton = ({ className, handler, text }) => (
    <Button onClick={handler} className={className}>
        {text}
    </Button>
);

export default DangerButton;

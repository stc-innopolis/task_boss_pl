const router = require('express').Router();

const stubs = {
    depts: 'data2',
    signUp: 'success'
};

const wait = (time) => (req, resp, next) => setTimeout(next, time);
wait.long = wait(1000);
wait.fast = wait(300);

router.post('/sign-up', wait.fast, (req, resp) => {
    resp.send(require(`./json/sign-up/${stubs.signUp}.json`));
});



router.post('/sign-up/verify', (req, resp) => {
    resp.send(require('./json/success.json'));
    // resp.send(require('../api/json/sign-up/failed-verification.json'));
});

router.post('/login', wait.fast, (req, resp) => {
    if (req.body.login === 'error') {
        resp.send(require('./json/login/illegal-credentials.json'));
    } else {
        resp.send(require('./json/login/success.json'));
    }
});

router.get('/depts', wait.long, (req, resp) => {
    resp.send(require('./json/depts/data2.json'));
});

router.get('/peoples', wait.long, (req, resp) => {
    resp.send(require('./json/depts/peoples/success.json'));
});

router.post('/depts/create', wait.long, (req, resp) => {
    resp.send(require('./json/depts/create-dept/success.json'));
    // eslint-disable-next-line import/no-dynamic-require
    resp.send(require(`./json/depts/${stubs.depts}.json`));
});

router.get('/tasks', (req, resp) => {
    setTimeout(() => {
        resp.send(require('./json/tasks/success.json'));
    }, 2000);
});

router.post('/tasks/create', (req, resp) => {
    setTimeout(() => {
        try {
            const fs = require('fs');
            const path = require('path');
            const md5 = require('crypto-js/md5');

            const { dept } = req.body;
            const rawTasksData = fs.readFileSync(path.resolve(__dirname, './json/tasks/success.json'));
            const tasksData = JSON.parse(rawTasksData);
            const deptTasksData = tasksData.body.filter((data) => data.label === dept);

            const task = {
                id: md5(new Date()).toString(),
                number: 100,
                task: req.body.task,
                status: 'open',
                priority: req.body.priority,
                performer: req.body.performer,
                deadline: new Date(req.body.deadline).getTime(),
                lastChanged: new Date().getTime(),
                description: req.body.description,
            };

            if (!deptTasksData.length) {
                const newDeptTasksData = {
                    label: req.body.dept,
                    data: [task],
                };
                tasksData.body.push(newDeptTasksData);
            } else {
                tasksData.body.map((data) => {
                    if (data.label === dept) {
                        return data.data.push(task);
                    }
                    return data;
                });
            }
            fs.writeFileSync(path.resolve(__dirname, './json/tasks/success.json'), JSON.stringify(tasksData, null, 4));
            resp.send({
                success: true,
                body: task,
                errors: [],
                warnings: [],
            });
        } catch (e) {
            console.log('error!');
        }
    }, 5000);
});

router.post('/tasks/delete', (req, resp) => {
    resp.send(require('./json/tasks/delete.json'));
});

router.post('/tasks/edit', wait(5000), (req, resp) => {
    resp.send({
        success: true,
        body: req.body,
    });
});

const workflow = require('./workflow.json');

router.use('/workflow', (req, resp) => {
    const { cmd, name } = req.query;
    let fName;
    let sName;
    let sStep = false;
    let eStep = false;

    if (cmd === 'start') {
        if (req?.session?.workflow?.flowName && req?.session?.workflow?.stateName) {
            fName = req?.session?.workflow?.flowName;
            sName = req?.session?.workflow?.stateName;
        } else {
            fName = name;
            sName = workflow[name].initState;
        }
    }

    if (cmd === 'event') {
        fName = req.session.workflow.flowName;
        const currentState = req.session.workflow.stateName;
        sName = workflow[fName].states[currentState].events[name].nextState;
    }

    req.session.workflow = {
        flowName: fName,
        stateName: sName,
    };

    if (workflow[fName].states[req.session.workflow.stateName].events.next === undefined) {
        eStep = true;
    }

    if (workflow[fName].states[req.session.workflow.stateName].events.back === undefined) {
        sStep = true;
    }

    resp.send({
        flowName: req.session.workflow.flowName,
        stateName: req.session.workflow.stateName,
        startStep: sStep,
        endStep: eStep,
    });
});

module.exports = router;

router.get('/admin/:key/:value', (req, resp) => {
    stubs[req.params.key] = req.params.value;
    resp.send(null);
});

router.get('/admin', (req, resp) => {
    resp.send(`
        <html>
            <head></head>
            <body>
                <section>
                    <h2>Департаменты</h2>
                    <ul>
                        <li><button onclick="fetch('/api/admin/depts/data2')">Data2</button></li>
                        <li><button onclick="fetch('/api/admin/depts/error')">Error</button></li>
                    </ul>
                    <h2>SignUp</h2>
                    <ul>
                        <li><button onclick="fetch('/api/admin/signUp/success')">Success</button></li>
                        <li><button onclick="fetch('/api/admin/signUp/error')">Error</button></li>
                    </ul>
                </section>
            </body>
        </html>
    `);
});

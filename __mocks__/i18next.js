const locales = require('../locales/ru.json');

module.exports = {
    t: (key) => locales[key],
};

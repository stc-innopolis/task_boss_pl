const ijlConfig = require('../ijl.config');

module.exports = {
    getConfigValue: (key) => ijlConfig.config[key],
    getConfig: () => ijlConfig.config,
    getFeatures: (key) => ijlConfig.features[key],
};

import React, { forwardRef } from 'react';

const ReactSelect = ({ selectRef, options, value, defaultValue, onChange, id }) => {
    function handleChange() {
        onChange(options[0]);
    }

    return (
        <select ref={selectRef} data-testid="select" id={id} value={value} onChange={handleChange}>
            {options?.map(({ label, value }) => (
                <option key={value} value={value || defaultValue}>
                    {label}
                </option>
            ))}
        </select>
    );
};

module.exports = forwardRef((props, ref) => <ReactSelect {...props} selectRef={ref} />);
